package com.itto.farmos.printer.port.bluetooth;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Iterator;
import java.util.Vector;

import com.citizen.jpos.command.CPCLConst;
import com.citizen.jpos.command.ESCPOS;
import com.citizen.jpos.printer.CPCLPrinter;
import com.citizen.jpos.printer.ESCPOSPrinter;
import com.citizen.port.android.BluetoothPort;
import com.citizen.request.android.RequestHandler;
import com.itto.farmos.R;
import com.itto.farmos.printer.assist.AlertView;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

/**
 * BluetoothConnectActivity
 *
 * @author Sung-Keun Lee
 * @version 2011. 12. 21.
 */
public class BluetoothConnectActivity extends AppCompatActivity {
    private static final String TAG = "BluetoothConnect";
    // Intent request codes
    // private static final int REQUEST_CONNECT_DEVICE = 1;
    private static final int REQUEST_ENABLE_BT = 2;

    ArrayAdapter<String> adapter;
    private BluetoothAdapter mBluetoothAdapter;
    private Vector<BluetoothDevice> remoteDevices;
    private BroadcastReceiver searchFinish;
    private BroadcastReceiver searchStart;
    private BroadcastReceiver discoveryResult;
    private Thread hThread;
    private Context context;
    // UI
    private EditText btAddrBox;
    private Button connectButton;
    private Button searchButton;
    private ListView list;
    // BT
    private BluetoothPort bluetoothPort;

    private static final String dir = Environment.getExternalStorageDirectory().getAbsolutePath() + "//temp";
    private static final String fileName = dir + "//BTPrinter";
    private String lastConnAddr;

    // Set up Bluetooth.
    private void bluetoothSetup() {
        // Initialize
        clearBtDevData();
        bluetoothPort = BluetoothPort.getInstance();
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            final int CODE = 5; // app defined constant used for onRequestPermissionsResult

            String[] permissionsToRequest =
                    {
                            Manifest.permission.BLUETOOTH_ADMIN,
                            Manifest.permission.BLUETOOTH,
                            Manifest.permission.ACCESS_FINE_LOCATION,
                            Manifest.permission.ACCESS_COARSE_LOCATION
                    };

            boolean allPermissionsGranted = true;

            for(String permission : permissionsToRequest)
            {
                allPermissionsGranted = allPermissionsGranted && (ContextCompat.checkSelfPermission(this, permission) == PackageManager.PERMISSION_GRANTED);
            }

            if(!allPermissionsGranted)
            {
                ActivityCompat.requestPermissions(this, permissionsToRequest, CODE);
            }
        }
        if (mBluetoothAdapter == null) {
            // Device does not support Bluetooth
            return;
        }
        if (!mBluetoothAdapter.isEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
        }
    }


    // clear device data used list.
    private void clearBtDevData() {
        remoteDevices = new Vector<BluetoothDevice>();
    }

    // add paired device to list
    private void addPairedDevices() {
        BluetoothDevice pairedDevice;
        Iterator<BluetoothDevice> iter = (mBluetoothAdapter.getBondedDevices()).iterator();
        while (iter.hasNext()) {
            pairedDevice = iter.next();
            remoteDevices.add(pairedDevice);
            adapter.add(pairedDevice.getName() + "\n[" + pairedDevice.getAddress() + "] [Paired]");
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.bluetooth_menu);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        // Setting
        btAddrBox = (EditText) findViewById(R.id.EditTextAddressBT);
        connectButton = (Button) findViewById(R.id.ButtonConnectBT);
        searchButton = (Button) findViewById(R.id.ButtonSearchBT);

        list = (ListView) findViewById(R.id.ListView01);
        context = this;
        // Setting
        bluetoothSetup();
        // Connect, Disconnect -- Button
        connectButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!bluetoothPort.isConnected()) // Connect routine.
                {
                    try {
                        btConn(mBluetoothAdapter.getRemoteDevice(btAddrBox.getText().toString()));
                    } catch (IllegalArgumentException e) {
                        // Bluetooth Address Format [OO:OO:OO:OO:OO:OO]
                        Log.e(TAG, e.getMessage(), e);
                        AlertView.showAlert(e.getMessage(), context);
                        return;
                    } catch (IOException e) {
                        Log.e(TAG, e.getMessage(), e);
                        AlertView.showAlert(e.getMessage(), context);
                        return;
                    }
                } else // Disconnect routine.
                {
                    // Always run.
                    btDisconn();
                }
            }
        });
        // Search Button
        searchButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!mBluetoothAdapter.isDiscovering()) {
                    clearBtDevData();
                    adapter.clear();
                    int MY_PERMISSIONS_REQUEST_ACCESS_COARSE_LOCATION = 1;
                    ActivityCompat.requestPermissions(BluetoothConnectActivity.this,
                            new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
                            MY_PERMISSIONS_REQUEST_ACCESS_COARSE_LOCATION);
                    mBluetoothAdapter.startDiscovery();
                } else {
                    mBluetoothAdapter.cancelDiscovery();
                }
            }
        });
        // Bluetooth Device List
        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1);
        list.setAdapter(adapter);
        addPairedDevices();
        // Connect - click the List item.
        list.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
                BluetoothDevice btDev = remoteDevices.elementAt(arg2);
                try {
                    if (mBluetoothAdapter.isDiscovering()) {
                        mBluetoothAdapter.cancelDiscovery();
                    }
                    btAddrBox.setText(btDev.getAddress());
                    btConn(btDev);
                } catch (IOException e) {
                    AlertView.showAlert(e.getMessage(), context);
                    return;
                }
            }
        });

        // UI - Event Handler.
        // Search device, then add List.
        discoveryResult = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String key;
                BluetoothDevice remoteDevice = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                if (remoteDevice != null) {
                    if (remoteDevice.getBondState() != BluetoothDevice.BOND_BONDED) {
                        key = remoteDevice.getName() + "\n[" + remoteDevice.getAddress() + "]";
                    } else {
                        key = remoteDevice.getName() + "\n[" + remoteDevice.getAddress() + "] [Paired]";
                    }
                    remoteDevices.add(remoteDevice);
                    adapter.add(key);
                }
            }
        };
        registerReceiver(discoveryResult, new IntentFilter(BluetoothDevice.ACTION_FOUND));
        searchStart = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                connectButton.setEnabled(false);
                btAddrBox.setEnabled(false);
                searchButton.setText(getResources().getString(R.string.bt_stop_search_btn));
            }
        };
        registerReceiver(searchStart, new IntentFilter(BluetoothAdapter.ACTION_DISCOVERY_STARTED));
        searchFinish = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                connectButton.setEnabled(true);
                btAddrBox.setEnabled(true);
                searchButton.setText(getResources().getString(R.string.bt_search_btn));
            }
        };
        registerReceiver(searchFinish, new IntentFilter(BluetoothAdapter.ACTION_DISCOVERY_FINISHED));
    }

    @Override
    protected void onDestroy() {
//        try {
//            bluetoothPort.disconnect();
//        } catch (IOException e) {
//            Log.e(TAG, e.getMessage(), e);
//        } catch (InterruptedException e) {
//            Log.e(TAG, e.getMessage(), e);
//        }
//        if ((hThread != null) && (hThread.isAlive())) {
//            hThread.interrupt();
//            hThread = null;
//        }
        unregisterReceiver(searchFinish);
        unregisterReceiver(searchStart);
        unregisterReceiver(discoveryResult);
        super.onDestroy();
    }

    private connTask connectionTask;

    // Bluetooth Connection method.
    private void btConn(final BluetoothDevice btDev) throws IOException {
        if ((connectionTask != null) && (connectionTask.getStatus() == AsyncTask.Status.RUNNING)) {
            connectionTask.cancel(true);
            if (!connectionTask.isCancelled())
                connectionTask.cancel(true);
            connectionTask = null;
        }
        connectionTask = new connTask();
        connectionTask.execute(btDev);
    }

    // Bluetooth Disconnection method.
    private void btDisconn() {
        try {
            bluetoothPort.disconnect();
        } catch (Exception e) {
            Log.e(TAG, e.getMessage(), e);
        }
        if ((hThread != null) && (hThread.isAlive()))
            hThread.interrupt();
        // UI
        connectButton.setText(getResources().getString(R.string.dev_conn_btn));
        list.setEnabled(true);
        btAddrBox.setEnabled(true);
        searchButton.setEnabled(true);
        Toast toast = Toast.makeText(context, getResources().getString(R.string.bt_disconn_msg), Toast.LENGTH_SHORT);
        toast.show();
    }

    // Bluetooth Connection Task.
    class connTask extends AsyncTask<BluetoothDevice, Void, Integer> {
        private final ProgressDialog dialog = new ProgressDialog(BluetoothConnectActivity.this);

        @Override
        protected void onPreExecute() {
            dialog.setTitle(getResources().getString(R.string.bt_tab));
            dialog.setMessage(getResources().getString(R.string.connecting_msg));
            dialog.show();
            super.onPreExecute();
        }

        @Override
        protected Integer doInBackground(BluetoothDevice... params) {
            Integer retVal = null;
            try {
                bluetoothPort.connect(params[0]);
                lastConnAddr = params[0].getAddress();
                retVal = Integer.valueOf(0);
            } catch (IOException e) {
                retVal = Integer.valueOf(-1);
            }
            return retVal;
        }

        @Override
        protected void onPostExecute(Integer result) {
            if (result.intValue() == 0)    // Connection success.
            {
                RequestHandler rh = new RequestHandler();
                hThread = new Thread(rh);
                hThread.start();
                // UI
                connectButton.setText(getResources().getString(R.string.dev_disconn_btn));
                list.setEnabled(false);
                btAddrBox.setEnabled(false);
                searchButton.setEnabled(false);
                if (dialog.isShowing())
                    dialog.dismiss();
                Toast toast = Toast.makeText(context, getResources().getString(R.string.bt_conn_msg), Toast.LENGTH_SHORT);
                toast.show();
            } else    // Connection failed.
            {
                if (dialog.isShowing())
                    dialog.dismiss();
                AlertView.showAlert(getResources().getString(R.string.bt_conn_fail_msg),
                        getResources().getString(R.string.dev_check_msg), context);
            }
            super.onPostExecute(result);
        }
    }
}