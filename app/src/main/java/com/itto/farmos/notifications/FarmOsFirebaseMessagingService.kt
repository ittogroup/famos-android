package com.itto.farmos.notifications

import android.annotation.TargetApi
import android.app.Notification
import android.app.NotificationChannel
import android.content.Context
import android.util.Log
import com.google.firebase.messaging.RemoteMessage
import com.google.firebase.messaging.FirebaseMessagingService
import android.content.Intent
import android.app.NotificationManager
import com.itto.farmos.R.mipmap.ic_launcher
import android.media.RingtoneManager
import android.app.PendingIntent
import android.graphics.Color
import android.os.Build
import android.support.v4.app.NotificationCompat
import com.itto.farmos.R
import com.itto.farmos.activities.MainActivity






class FarmOsFirebaseMessagingService : FirebaseMessagingService() {

    override fun onMessageReceived(remoteMessage: RemoteMessage?) {

        Log.d(TAG, "From: " + remoteMessage!!.from!!)
        Log.d(TAG, "Notification Message Body: " + remoteMessage.notification!!.body!!)

        showNotif(remoteMessage.notification!!.body!!)

    }

    @TargetApi(Build.VERSION_CODES.O)
    private fun showNotif(message: String){
        val manager: NotificationManager by lazy {
            getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        }
        val chan2 = NotificationChannel(PRIMARY_CHANNEL,
                getString(R.string.default_notification_channel_id), NotificationManager.IMPORTANCE_HIGH)
        chan2.lightColor = Color.BLUE
        chan2.lockscreenVisibility = Notification.VISIBILITY_PUBLIC
        manager.createNotificationChannel(chan2)
        val not = Notification.Builder(applicationContext, PRIMARY_CHANNEL)
                .setContentTitle(message)
                .setContentText(message)
//                .setSmallIcon(smallIcon)
                .setAutoCancel(true)
    }

    private fun showNotification(message: String) {

        //        MainActivity.start(this,message);

        val intent = Intent(this, MainActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)

        val pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT)
        val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)

        val notificationBuilder = NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle("Tuấn Đẹp Trai")
                .setContentText(message)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent)

        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.notify(0, notificationBuilder.build())
    }
    fun start(context: Context) {
        val startServiceIntent = Intent(context, FirebaseIDService::class.java)
        context.startService(startServiceIntent)

        val notificationServiceIntent = Intent(context, FirebaseIDService::class.java)
        context.startService(notificationServiceIntent)
    }

    companion object {
        private val TAG = "FCM Service"
        val PRIMARY_CHANNEL = "default"
    }

}