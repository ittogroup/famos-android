package com.itto.farmos.notifications

import android.util.Log
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.iid.FirebaseInstanceIdService
import com.itto.farmos.config.Constants
import com.itto.farmos.config.UserData
import com.itto.farmos.models.FcmToken
import com.itto.farmos.services.FcmTokenApi
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers


class FirebaseIDService : FirebaseInstanceIdService() {

    override fun onTokenRefresh() {
        // Get updated InstanceID token.
        val refreshedToken = FirebaseInstanceId.getInstance().token

        sendRegistrationToServer(refreshedToken)
    }

    /**
     * Persist token to third-party servers.
     *
     * Modify this method to associate the user's FCM InstanceID token with any server-side account
     * maintained by your application.
     *
     * @param token The new token.
     */
    private fun sendRegistrationToServer(token: String?) {
        if (UserData.instance.currentUser == null) {
            return
        }
        val fcmTokenApi = FcmTokenApi.create()
        val prefs = getSharedPreferences(Constants.SHARED_PREFS, 0)
        val userToken = prefs.getString(Constants.USER_TOKEN, "")
        val fcmToken = FcmToken(
                token = token,
                user_id = UserData.instance.currentUser!!.username
        )

        fcmTokenApi.sendFcmToken("token  $userToken", fcmToken).observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({ fcmTokenResponse ->
                    Log.i(TAG, fcmTokenResponse.status)
                }, { error ->


                    error.printStackTrace()
                })
    }

    companion object {
        private val TAG = "FirebaseIDService"
    }
}