package com.itto.farmos.dialogs

import android.app.Dialog
import android.content.Context
import android.view.View
import android.widget.*
import com.itto.farmos.R
import com.itto.farmos.adapters.VarietyAdapter
import com.itto.farmos.tools.LoaderData
import com.itto.farmos.tools.OnSearch
import kotlinx.android.synthetic.main.search_dialog.*

class SearchDialog(context: Context) : Dialog(context) {
    private var selectedVarietyId: Long = 0

    fun searchDialog(searchDelegate: OnSearch) {
        val dialog = Dialog(context)
        selectedVarietyId = 0
        dialog.setContentView(R.layout.search_dialog)
        dialog.setCancelable(false)
        val varietyAdapter = VarietyAdapter(context, LoaderData.instance.varieties)
        dialog.varietyTextView.setAdapter(varietyAdapter)
        dialog.varietyTextView.threshold = 0
        dialog.varietyTextView.setOnItemClickListener { _: AdapterView<*>, _: View, _: Int, id: Long ->
            selectedVarietyId = id
        }

        val btDialog: Button = dialog.findViewById(R.id.btDialog)
        btDialog.setOnClickListener({

            searchDelegate.seachStock(dialog.palletid.text.toString(), selectedVarietyId.toString(),
                    dialog.unitsText.text.toString(), dialog.weightText.text.toString(),
                    dialog.corporalText.text.toString())

            dialog.dismiss()
        })

        val btCloseSearch: Button = dialog.findViewById(R.id.btCloseSearch)
        btCloseSearch.setOnClickListener({

            dialog.dismiss()
        })

        dialog.show()
    }

}