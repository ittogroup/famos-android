package com.itto.farmos.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.Filter
import android.widget.Filterable
import android.widget.TextView
import com.itto.farmos.R
import com.itto.farmos.models.Variety


class VarietyAdapter(val context: Context, var varieties: List<Variety>?) : BaseAdapter(), Filterable {
    private var vi: LayoutInflater? = null
    private var filteredList: ArrayList<Variety>? = null

    init {
        filteredList = null
        vi = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
    }
    override fun getView(position: Int, v: View?, parent: ViewGroup?): View {
        var holder: ViewHolder
        var retView: View

        if(v == null){
            retView = vi!!.inflate(R.layout.simple_list_row, null)
            holder = ViewHolder()

            holder.title = retView.findViewById(R.id.title)
            retView.tag = holder

        } else {
            holder = v.tag as ViewHolder
            retView = v
        }

        holder.title!!.text = filteredList!![position].name + " [ " + filteredList!![position].parcel.name + " ]"

        return retView
    }

    override fun getItem(position : Int) : String {
        return filteredList!![position].name
    }

    override fun getCount() : Int {
        if(filteredList == null)
            return 0

        return filteredList!!.size
    }

    override fun getItemId(position : Int) : Long {
        return filteredList!![position].id
    }

    internal class ViewHolder {
        var title: TextView? = null
    }

    override fun getFilter(): Filter {
       return VarietyFilter()
    }

    private inner class VarietyFilter : Filter() {
        override fun publishResults(constraint: CharSequence?, results: Filter.FilterResults) {
            if (filteredList == null)
                filteredList = ArrayList<Variety>()

            filteredList!!.clear()
            val valFi = results.values
            if(valFi != null) {
                filteredList!!.addAll(valFi as List < Variety >)
            }
            notifyDataSetChanged()
        }

        override fun performFiltering(constraint: CharSequence?): Filter.FilterResults {
            val results = Filter.FilterResults()
            val selected = varieties!!.filter {it.name.contains(constraint.toString(), true)}
            results.count = selected.count()
            results.values = selected
            return results
        }

    }
}