package com.itto.farmos.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import com.itto.farmos.R
import com.itto.farmos.models.ProcessedStock
import com.itto.farmos.models.Stock
import com.itto.farmos.tools.HTML
import com.itto.farmos.tools.OnBottomReachedListener
import com.itto.farmos.tools.OnClickItem
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols
import java.util.*

class ProcessedStocksAdapter(var processedStocks: List<ProcessedStock>?, var context: Context, var onClickItem: OnClickItem) : RecyclerView.Adapter<ProcessedStocksAdapter.StockViewHolder>() {
    var selectedItemCount: Int = 0
    var selectedArchive: ArrayList<Int>? =  ArrayList()
    var onBottomReachedListener: OnBottomReachedListener? = null

    inner class StockViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var title: TextView = view.findViewById(R.id.processingVarietyText)
        var age: TextView = view.findViewById(R.id.processingAgeText)
        var weight: TextView = view.findViewById(R.id.processingWeight)
        var archiveButton: ImageButton = view.findViewById(R.id.archiveButton)
        var palletId: TextView = view.findViewById(R.id.processingPalletId)
        var selected: Boolean = false
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StockViewHolder {
        val itemView = LayoutInflater.from(parent.context)
                .inflate(R.layout.processing_list_row, parent, false)

        return StockViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: StockViewHolder, position: Int) {
        val processedStock = processedStocks!![position]
        val stock = processedStock.stock
        holder.title.text = HTML.fromHtml(stock!!.variety!!.name + " <br> <b>[ "+ stock!!.variety!!.parcel.name +" ]</b>")
        holder.palletId.text =HTML.fromHtml(" Pallet Id :<b> "+ stock!!.id.toString() +" </b>")
        if (position == processedStocks!!.count() - 1){
            if(onBottomReachedListener != null) {
                onBottomReachedListener!!.onBottomReached(position)
            }

        }

        if (position % 2 == 0) {
            holder.itemView.setBackgroundColor(this@ProcessedStocksAdapter.context.resources.getColor(R.color.whiteSmoke))
        } else {
            holder.itemView.setBackgroundColor(this@ProcessedStocksAdapter.context.resources.getColor(R.color.white))

        }

        val longClickListener = { _: View ->
            if (selectedItemCount == 0) {
                if (!holder.selected) {
                    holder.itemView.setBackgroundColor(this@ProcessedStocksAdapter.context.resources.getColor(R.color.colorPrimaryDark))
                    holder.archiveButton.visibility = View.VISIBLE
                    holder.age.visibility = View.INVISIBLE
                    selectedItemCount += 1
                } else {
                    holder.archiveButton.visibility = View.GONE
                    holder.age.visibility = View.VISIBLE

                    if (position % 2 == 0) {
                        holder.itemView.setBackgroundColor(this@ProcessedStocksAdapter.context.resources.getColor(R.color.whiteSmoke))
                    } else {
                        holder.itemView.setBackgroundColor(this@ProcessedStocksAdapter.context.resources.getColor(R.color.white))

                    }
                    selectedItemCount -= 1
                }
                holder.selected = !holder.selected
                if(holder.selected ){
                    selectedArchive!!.add(processedStock.id!!.toInt())
                }else{
                    selectedArchive!!.remove(processedStock.id!!.toInt())
                }

            }
            onClickItem.badgeUpdate()
            true
        }

        holder.itemView.setOnLongClickListener(longClickListener)
        val clickItemListener = { _: View ->
            onClickItem.clickItem(position)

            if (selectedItemCount > 0) {
                if (!holder.selected) {
                    holder.itemView.setBackgroundColor(this@ProcessedStocksAdapter.context.resources.getColor(R.color.colorPrimaryDark))
                    holder.archiveButton.visibility = View.VISIBLE
                    holder.age.visibility = View.INVISIBLE
                    selectedItemCount += 1

                } else {
                    holder.archiveButton.visibility = View.GONE
                    holder.age.visibility = View.VISIBLE

                    if (position % 2 == 0) {
                        holder.itemView.setBackgroundColor(this@ProcessedStocksAdapter.context.resources.getColor(R.color.whiteSmoke))
                    } else {
                        holder.itemView.setBackgroundColor(this@ProcessedStocksAdapter.context.resources.getColor(R.color.white))

                    }
                    selectedItemCount -= 1
                }
                holder.selected = !holder.selected
                if(holder.selected ){
                    selectedArchive!!.add(processedStock.id!!.toInt())
                }else{
                    selectedArchive!!.remove(processedStock.id!!.toInt())
                }
            }
            onClickItem.badgeUpdate()
        }


        holder.itemView.setOnClickListener(clickItemListener)


        val netWeight = if(stock.packaging != null && stock.units != null && stock.weight != null){
            (stock.weight as Double - (stock.units as Int * stock.packaging!!.weight) - 30)
        }else{
            stock.weight
        }
        holder.weight.text = DecimalFormat("#.#", DecimalFormatSymbols(Locale.ENGLISH)).format(netWeight) + "Kg"

        val days = processedStock.age!! / 24
        var ageStr = "%dD".format(days)

        if (days == 0) {
            ageStr = "%dH".format(processedStock.age!! % 24)
        }
        holder.age.text = ageStr
    }

    override fun getItemCount(): Int {

        if (processedStocks == null)
            return 0

        return processedStocks!!.size
    }

    fun getItemByStock(pId: Long): ProcessedStock? {
        val selected = processedStocks!!.filter { it.stock!!.id == pId }
        if (selected.isEmpty()) {
            return null
        }

        return selected[0]
    }

    fun getItem(position: Int): ProcessedStock? {
        if (processedStocks == null)
            return null
        return processedStocks!![position]
    }

}