package com.itto.farmos.adapters

import android.content.Context
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import com.itto.farmos.R
import com.itto.farmos.fragments.PalletFragment
import com.itto.farmos.fragments.ProcessingFragment
import com.itto.farmos.fragments.StockFragment
import android.view.ViewGroup
import com.itto.farmos.fragments.BLFragment


class MainTabFragmentPageAdapter(fm: FragmentManager, var context: Context): FragmentPagerAdapter(fm){
    override fun getItem(position: Int): Fragment {
        when (position) {
            0 -> return StockFragment()
            1 -> return ProcessingFragment()
            2 -> return PalletFragment()
        }

        return BLFragment()

    }

    private var mCurrentFragment: Fragment? = null

    fun getCurrentFragment(): Fragment? {
        return mCurrentFragment
    }
    override fun getCount(): Int {
        return 4
    }

    override fun setPrimaryItem(container: ViewGroup?, position: Int, obj: Any) {
        if (getCurrentFragment() !== obj) {
            mCurrentFragment = obj as Fragment
            if(mCurrentFragment is PalletFragment){
                (mCurrentFragment as PalletFragment).fetchPallets(true)
            }else if(mCurrentFragment is StockFragment){
                (mCurrentFragment as StockFragment).fetchStocks(true)
            }else if(mCurrentFragment is ProcessingFragment){
                (mCurrentFragment as ProcessingFragment).fetchStocks(true)
            }else if(mCurrentFragment is BLFragment){
                (mCurrentFragment as BLFragment).fetchBLs(true)
            }
        }
        super.setPrimaryItem(container, position, obj)
    }

    override fun getPageTitle(position: Int): CharSequence {

        when (position) {
            0 -> return context.getString(R.string.stockTitle)
            1 -> return context.getString(R.string.processingTitle)
            2 -> return context.getString(R.string.PalletTitle)
        }

        return context.getString(R.string.BLTitle)

    }
}