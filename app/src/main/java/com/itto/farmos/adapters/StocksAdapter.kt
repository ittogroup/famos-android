package com.itto.farmos.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import com.itto.farmos.R
import com.itto.farmos.models.Stock
import com.itto.farmos.tools.HTML
import com.itto.farmos.tools.OnClickItem
import com.itto.farmos.tools.OnBottomReachedListener
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols
import java.util.*


class StocksAdapter(var stocks: List<Stock>?, var context: Context, var onClickItem: OnClickItem) : RecyclerView.Adapter<StocksAdapter.StockViewHolder>() {
        var selectedItemCount: Int = 0
        var selectedArchive: ArrayList<Int>? =  ArrayList()
        var onBottomReachedListener: OnBottomReachedListener? = null

    inner class StockViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            var title: TextView = view.findViewById(R.id.stockVarietyText)
            var age: TextView = view.findViewById(R.id.stockAgeText)
            var weight: TextView = view.findViewById(R.id.stockWeight)
            var archiveButton: ImageButton = view.findViewById(R.id.archiveButton)
            var isTemporary: View = view.findViewById(R.id.isTemporary)
            var palletId: TextView = view.findViewById(R.id.stockPalletId)
            var selected: Boolean = false
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StockViewHolder {
            val itemView = LayoutInflater.from(parent.context)
                    .inflate(R.layout.stock_list_row, parent, false)

            return StockViewHolder(itemView)
        }

        override fun onBindViewHolder(holder: StockViewHolder, position: Int) {
            val stock = stocks!![position]
            holder.title.text = HTML.fromHtml(stock.variety!!.name + " <br> <b>[ "+ stock.variety!!.parcel.name +" ]</b>")
            holder.palletId.text =HTML.fromHtml(" Pallet Id :<b> "+ stock!!.id.toString() +" </b>")
            if (position == stocks!!.count() - 1){
                if(onBottomReachedListener != null) {
                    onBottomReachedListener!!.onBottomReached(position)
                }

            }
            if (position % 2 == 0){
                holder.itemView.setBackgroundColor(this@StocksAdapter.context.resources.getColor(R.color.whiteSmoke))
            }else{
                holder.itemView.setBackgroundColor(this@StocksAdapter.context.resources.getColor(R.color.white))

            }

            val longClickListener = { _ : View ->
                if (selectedItemCount == 0) {
                    if (!holder.selected) {
                        holder.itemView.setBackgroundColor(this@StocksAdapter.context.resources.getColor(R.color.colorPrimaryDark))
                        holder.archiveButton.visibility = View.VISIBLE
                        holder.age.visibility = View.INVISIBLE
                        selectedItemCount += 1
                    } else {
                        holder.archiveButton.visibility = View.GONE
                        holder.age.visibility = View.VISIBLE

                        if (position % 2 == 0) {
                            holder.itemView.setBackgroundColor(this@StocksAdapter.context.resources.getColor(R.color.whiteSmoke))
                        } else {
                            holder.itemView.setBackgroundColor(this@StocksAdapter.context.resources.getColor(R.color.white))

                        }
                        selectedItemCount -= 1
                    }
                    holder.selected = !holder.selected
                    if(holder.selected ){
                        selectedArchive!!.add(stock.id!!.toInt())
                    }else{
                        selectedArchive!!.remove(stock.id!!.toInt())
                    }
                }
                onClickItem.badgeUpdate()

                true
            }



            holder.itemView.setOnLongClickListener(longClickListener)
            val clickItemListener = { _ : View ->
                onClickItem.clickItem(position)
                if (selectedItemCount > 0) {
                    if (!holder.selected) {
                        holder.itemView.setBackgroundColor(this@StocksAdapter.context.resources.getColor(R.color.colorPrimaryDark))
                        holder.archiveButton.visibility = View.VISIBLE
                        holder.age.visibility = View.INVISIBLE
                        selectedItemCount += 1

                    } else {
                        holder.archiveButton.visibility = View.GONE
                        holder.age.visibility = View.VISIBLE

                        if (position % 2 == 0) {
                            holder.itemView.setBackgroundColor(this@StocksAdapter.context.resources.getColor(R.color.whiteSmoke))
                        } else {
                            holder.itemView.setBackgroundColor(this@StocksAdapter.context.resources.getColor(R.color.white))

                        }
                        selectedItemCount -= 1
                    }
                    holder.selected = !holder.selected
                    if(holder.selected ){
                        selectedArchive!!.add(stock.id!!.toInt())
                    }else{
                        selectedArchive!!.remove(stock.id!!.toInt())
                    }
                }
                onClickItem.badgeUpdate()

            }


            holder.itemView.setOnClickListener(clickItemListener)

            var temporary = ""
            holder.isTemporary.visibility = View.INVISIBLE
            if (stock.temporary!!){
                temporary = this@StocksAdapter.context.getString(R.string.temporary)
                holder.isTemporary.visibility = View.VISIBLE
            }
            val netWeight = if(stock.packaging != null && stock.units != null && stock.weight != null){
                (stock.weight as Double - (stock.units as Int * stock.packaging!!.weight) - 30)
            }else{
                stock.weight
            }
            holder.weight.text = DecimalFormat("#.#", DecimalFormatSymbols(Locale.ENGLISH)).format(netWeight) + "Kg" + temporary
            var days = stock.age!!/ 24
            var ageStr = "%dD".format(days)
            if (days == 0){
                ageStr = "%dH".format(stock.age!! % 24)
            }
            holder.age.text = ageStr
        }

        override fun getItemCount(): Int {

            if (stocks == null)
                return 0

            return stocks!!.size
        }

        fun getItem(pId: Long): Stock?{
            val selected = stocks!!.filter {it.id == pId}
            if (selected.isEmpty()){
                return null
            }

            return selected[0]
        }

        fun getItem(position: Int): Stock?{
            if (stocks == null)
                return null
           return  stocks!![position]
        }

}