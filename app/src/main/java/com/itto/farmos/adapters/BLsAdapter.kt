package com.itto.farmos.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import com.itto.farmos.R
import com.itto.farmos.models.BL
import com.itto.farmos.models.Pallet
import com.itto.farmos.models.PalletEntry
import com.itto.farmos.models.Stock
import com.itto.farmos.tools.HTML
import com.itto.farmos.tools.OnBottomReachedListener
import com.itto.farmos.tools.OnClickItem
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols
import java.util.*

class BLsAdapter(var bls: List<BL>?, var context: Context, var onClickItem: OnClickItem) : RecyclerView.Adapter<BLsAdapter.PalletViewHolder>() {
    var selectedItemCount: Int = 0
    var selectedArchive: ArrayList<Int>? =  ArrayList()
    var onBottomReachedListener: OnBottomReachedListener? = null

    inner class PalletViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var blClientTextView: TextView = view.findViewById(R.id.blClientText)
        var ageTextView: TextView = view.findViewById(R.id.blAgeText)
        var totalTextView: TextView = view.findViewById(R.id.blTotal)
        var archiveButton: ImageButton = view.findViewById(R.id.archiveButton)
        var blIdTextView: TextView = view.findViewById(R.id.BLId)

        var selected: Boolean = false
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PalletViewHolder {
        val itemView = LayoutInflater.from(parent.context)
                .inflate(R.layout.bl_list_row, parent, false)

        return PalletViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: PalletViewHolder, position: Int) {
        val bl = bls!![position]
        holder.blIdTextView.text =HTML.fromHtml(" BL Id :<b> "+ bl.id.toString() +" </b>")

        holder.blClientTextView.text = bl.client!!.name.toString()
        if (position == bls!!.count() - 1){
            if(onBottomReachedListener != null) {
                onBottomReachedListener!!.onBottomReached(position)
            }

        }
        if (position % 2 == 0) {
            holder.itemView.setBackgroundColor(this@BLsAdapter.context.resources.getColor(R.color.whiteSmoke))
        } else {
            holder.itemView.setBackgroundColor(this@BLsAdapter.context.resources.getColor(R.color.white))

        }

        val longClickListener = { _ : View ->
            if (selectedItemCount == 0) {
                if (!holder.selected) {
                    holder.itemView.setBackgroundColor(this@BLsAdapter.context.resources.getColor(R.color.colorPrimaryDark))
                    holder.archiveButton.visibility = View.VISIBLE
                    holder.ageTextView.visibility = View.INVISIBLE
                    selectedItemCount += 1
                } else {
                    holder.archiveButton.visibility = View.GONE
                    holder.ageTextView.visibility = View.VISIBLE

                    if (position % 2 == 0) {
                        holder.itemView.setBackgroundColor(this@BLsAdapter.context.resources.getColor(R.color.whiteSmoke))
                    } else {
                        holder.itemView.setBackgroundColor(this@BLsAdapter.context.resources.getColor(R.color.white))

                    }
                    selectedItemCount -= 1
                }
                holder.selected = !holder.selected
                if(holder.selected ){
                    selectedArchive!!.add(bl.id!!.toInt())
                }else{
                    selectedArchive!!.remove(bl.id!!.toInt())
                }
            }
            onClickItem.badgeUpdate()
            true
        }



        holder.itemView.setOnLongClickListener(longClickListener)
        val clickItemListener = { _: View ->
            onClickItem.clickItem(position)

            if (selectedItemCount > 0) {
                if (!holder.selected) {
                    holder.itemView.setBackgroundColor(this@BLsAdapter.context.resources.getColor(R.color.colorPrimaryDark))
                    holder.archiveButton.visibility = View.VISIBLE
                    holder.ageTextView.visibility = View.INVISIBLE
                    selectedItemCount += 1

                } else {
                    holder.archiveButton.visibility = View.GONE
                    holder.ageTextView.visibility = View.VISIBLE

                    if (position % 2 == 0) {
                        holder.itemView.setBackgroundColor(this@BLsAdapter.context.resources.getColor(R.color.whiteSmoke))
                    } else {
                        holder.itemView.setBackgroundColor(this@BLsAdapter.context.resources.getColor(R.color.white))

                    }
                    selectedItemCount -= 1
                }
                holder.selected = !holder.selected
                if(holder.selected ){
                    selectedArchive!!.add(bl.id!!.toInt())
                }else{
                    selectedArchive!!.remove(bl.id!!.toInt())
                }
            }
            onClickItem.badgeUpdate()

        }

        holder.itemView.setOnClickListener(clickItemListener)


        var days = bl.age!! / 24
        var ageStr = "%dD".format(days)
        if (days == 0) {
            ageStr = "%dH".format(bl.age!! % 24)
        }
        holder.ageTextView.text = ageStr
    }

    override fun getItemCount(): Int {

        if (bls == null)
            return 0

        return bls!!.size
    }

    fun getItem(pId: Long): BL? {
        val selected = bls!!.filter { it.id == pId }
        if (selected.isEmpty()) {
            return null
        }

        return selected[0]
    }

    fun getItem(position: Int): BL? {
        if (bls == null)
            return null
        return bls!![position]
    }

}