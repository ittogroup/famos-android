package com.itto.farmos.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import com.itto.farmos.R
import com.itto.farmos.models.Pallet
import com.itto.farmos.models.PalletEntry
import com.itto.farmos.models.Stock
import com.itto.farmos.tools.HTML
import com.itto.farmos.tools.OnBottomReachedListener
import com.itto.farmos.tools.OnClickItem
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols
import java.util.*

class PalletsAdapter(var pallets: List<Pallet>?, var context: Context, var onClickItem: OnClickItem) : RecyclerView.Adapter<PalletsAdapter.PalletViewHolder>() {
    var selectedItemCount: Int = 0
    var selectedArchive: ArrayList<Int>? =  ArrayList()
    var onBottomReachedListener: OnBottomReachedListener? = null

    inner class PalletViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var title: TextView = view.findViewById(R.id.palletVarietyText)
        var age: TextView = view.findViewById(R.id.palletAgeText)
        var weight: TextView = view.findViewById(R.id.palletWeight)
        var archiveButton: ImageButton = view.findViewById(R.id.archiveButton)
        var destination: TextView = view.findViewById(R.id.destination)
        var palletId: TextView = view.findViewById(R.id.palletIdText)

        var selected: Boolean = false
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PalletViewHolder {
        val itemView = LayoutInflater.from(parent.context)
                .inflate(R.layout.pallet_list_row, parent, false)

        return PalletViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: PalletViewHolder, position: Int) {
        val pallet = pallets!![position]
        holder.palletId.text =HTML.fromHtml(" Pallet Id :<b> "+ pallet.id.toString() +" </b>")

        holder.title.text = pallet.variety.parcel.product.name
        if (position == pallets!!.count() - 1){
            if(onBottomReachedListener != null) {
                onBottomReachedListener!!.onBottomReached(position)
            }

        }
        if (position % 2 == 0) {
            holder.itemView.setBackgroundColor(this@PalletsAdapter.context.resources.getColor(R.color.whiteSmoke))
        } else {
            holder.itemView.setBackgroundColor(this@PalletsAdapter.context.resources.getColor(R.color.white))

        }
        if (pallet!!.destination != null) {
            holder.destination.text = pallet.destination!!.name
            holder.destination.visibility = View.VISIBLE
        }else{
            holder.destination.visibility = View.GONE
        }
        val longClickListener = { _ : View ->
            if (selectedItemCount == 0) {
                if (!holder.selected) {
                    holder.itemView.setBackgroundColor(this@PalletsAdapter.context.resources.getColor(R.color.colorPrimaryDark))
                    holder.archiveButton.visibility = View.VISIBLE
                    holder.age.visibility = View.INVISIBLE
                    selectedItemCount += 1
                } else {
                    holder.archiveButton.visibility = View.GONE
                    holder.age.visibility = View.VISIBLE

                    if (position % 2 == 0) {
                        holder.itemView.setBackgroundColor(this@PalletsAdapter.context.resources.getColor(R.color.whiteSmoke))
                    } else {
                        holder.itemView.setBackgroundColor(this@PalletsAdapter.context.resources.getColor(R.color.white))

                    }
                    selectedItemCount -= 1
                }
                holder.selected = !holder.selected
                if(holder.selected ){
                    selectedArchive!!.add(pallet.id.toInt())
                }else{
                    selectedArchive!!.remove(pallet.id.toInt())
                }
            }
            onClickItem.badgeUpdate()
            true
        }



        holder.itemView.setOnLongClickListener(longClickListener)
        val clickItemListener = { _: View ->
            onClickItem.clickItem(position)

            if (selectedItemCount > 0) {
                if (!holder.selected) {
                    holder.itemView.setBackgroundColor(this@PalletsAdapter.context.resources.getColor(R.color.colorPrimaryDark))
                    holder.archiveButton.visibility = View.VISIBLE
                    holder.age.visibility = View.INVISIBLE
                    selectedItemCount += 1

                } else {
                    holder.archiveButton.visibility = View.GONE
                    holder.age.visibility = View.VISIBLE

                    if (position % 2 == 0) {
                        holder.itemView.setBackgroundColor(this@PalletsAdapter.context.resources.getColor(R.color.whiteSmoke))
                    } else {
                        holder.itemView.setBackgroundColor(this@PalletsAdapter.context.resources.getColor(R.color.white))

                    }
                    selectedItemCount -= 1
                }
                holder.selected = !holder.selected
                if(holder.selected ){
                    selectedArchive!!.add(pallet.id.toInt())
                }else{
                    selectedArchive!!.remove(pallet.id.toInt())
                }
            }
            onClickItem.badgeUpdate()

        }

        holder.itemView.setOnClickListener(clickItemListener)

        holder.weight.text = ""
        if(pallet.vrac!!){
            holder.weight.text = "[= Vrac =] "
        }
        var separator = ""
        var weightHtml =""
        for (palletEntry: PalletEntry in pallet.entries) {
            val netWeight = if(palletEntry.packaging != null && palletEntry.units != null && palletEntry.weight != null){
                (palletEntry.weight as Double - (palletEntry.units as Int * palletEntry.packaging!!.weight) - 18)
            }else{
                palletEntry.weight
            }

            val weightStr = DecimalFormat("#.#", DecimalFormatSymbols(Locale.ENGLISH)).format(netWeight)

            weightHtml = if(pallet.vrac!!){
                Log.i("tag :eroor  ", weightStr)
                weightHtml + separator + context.getString(R.string.palletVracWeight).format(weightStr)
            }else {
                weightHtml + separator + context.getString(R.string.palletCaliberWeight).format(palletEntry.calibre, weightStr)
            }
            separator = "<br> "
        }

        holder.weight.text = HTML.fromHtml(weightHtml)
        var days = pallet.age!! / 24
        var ageStr = "%dD".format(days)
        if (days == 0) {
            ageStr = "%dH".format(pallet.age!! % 24)
        }
        holder.age.text = ageStr
    }

    override fun getItemCount(): Int {

        if (pallets == null)
            return 0

        return pallets!!.size
    }

    fun getItem(pId: Long): Pallet? {
        val selected = pallets!!.filter { it.id == pId }
        if (selected.isEmpty()) {
            return null
        }

        return selected[0]
    }

    fun getItem(position: Int): Pallet? {
        if (pallets == null)
            return null
        return pallets!![position]
    }

}