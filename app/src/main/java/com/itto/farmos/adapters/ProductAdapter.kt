package com.itto.farmos.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.Filter
import android.widget.Filterable
import android.widget.TextView
import com.itto.farmos.R
import com.itto.farmos.models.Parcel
import com.itto.farmos.models.Product

class ProductAdapter(val context: Context, var products: List<Product>?) : BaseAdapter(), Filterable {
    private var vi: LayoutInflater? = null
    private var filteredList: ArrayList<Product>? = null

    init {
        filteredList = null
        vi = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
    }
    override fun getView(position: Int, v: View?, parent: ViewGroup?): View {
        var holder: ViewHolder
        var retView: View

        if(v == null){
            retView = vi!!.inflate(R.layout.simple_list_row, null)
            holder = ViewHolder()

            holder.title = retView.findViewById(R.id.title)
            retView.tag = holder

        } else {
            holder = v.tag as ViewHolder
            retView = v
        }

        holder.title!!.text = filteredList!![position].name

        return retView
    }



    override fun getItem(position : Int) : String {
        return filteredList!![position].name
    }

    override fun getCount() : Int {
        if(filteredList == null)
            return 0

        return filteredList!!.size
    }

    override fun getItemId(position : Int) : Long {
        return filteredList!![position].id
    }

    internal class ViewHolder {
        var title: TextView? = null
    }

    override fun getFilter(): Filter {
       return ProductFilter()
    }

    private inner class ProductFilter : Filter() {
        override fun publishResults(constraint: CharSequence?, results: FilterResults) {
            if (filteredList == null)
                filteredList = ArrayList()

            filteredList!!.clear()
            filteredList!!.addAll(results.values as List<Product>)
            notifyDataSetChanged()
        }

        override fun performFiltering(constraint: CharSequence?): FilterResults {
            val results = FilterResults()
            val selected = products!!.filter { it.name.contains(constraint.toString(), true)}
            results.count = selected.count()
            results.values = selected
            return results
        }

    }
}