package com.itto.farmos.models

import android.os.Parcelable
import com.fasterxml.jackson.annotation.JsonProperty


data class Product(
        @JsonProperty("id")
        var id: Long,

        @JsonProperty("name")
        var name: String,


        @JsonProperty("code")
        var code: String

): Parcelable {
    constructor(pParcel: android.os.Parcel) : this(
            pParcel.readLong(),
            pParcel.readString(),
            pParcel.readString()) {
    }

    override fun writeToParcel(pParcel: android.os.Parcel, flags: Int) {
        pParcel.writeLong(id)
        pParcel.writeString(name)
        pParcel.writeString(code)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Product> {
        override fun createFromParcel(parcel: android.os.Parcel): Product {
            return Product(parcel)
        }

        override fun newArray(size: Int): Array<Product?> {
            return arrayOfNulls(size)
        }
    }
}