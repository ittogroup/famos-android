package com.itto.farmos.models

import android.os.Parcel
import android.os.Parcelable
import com.fasterxml.jackson.annotation.JsonFormat
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty
import java.util.*

@JsonInclude(JsonInclude.Include.NON_NULL)
data class BLResult(
        @JsonProperty("count")
        val count: Int?,
        @JsonProperty("next")
        val next: String?,
        @JsonProperty("previous")
        val previous: String?,
        @JsonProperty("results")
        var results: List<BL>?

): Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readString(),
            parcel.readString(),
            parcel.createTypedArrayList(BL)) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeValue(count)
        parcel.writeString(next)
        parcel.writeString(previous)
        parcel.writeTypedList(results)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<BLResult> {
        override fun createFromParcel(parcel: Parcel): BLResult {
            return BLResult(parcel)
        }

        override fun newArray(size: Int): Array<BLResult?> {
            return arrayOfNulls(size)
        }
    }

}