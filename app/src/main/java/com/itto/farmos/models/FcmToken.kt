package com.itto.farmos.models

import com.fasterxml.jackson.annotation.JsonProperty

data class FcmToken(
        @JsonProperty("token")
        val token: String?,
        @JsonProperty("user_id")
        val user_id: String

)