package com.itto.farmos.models

import com.fasterxml.jackson.annotation.JsonProperty

data class FcmTokenResponse(
        @JsonProperty("token")
        val token: String?,
        @JsonProperty("user_id")
        val userId: String ,
        @JsonProperty("status")
        val status: String

)