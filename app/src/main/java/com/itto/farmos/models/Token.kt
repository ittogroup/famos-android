package com.itto.farmos.models

import com.fasterxml.jackson.annotation.JsonProperty

data class Token(
        @JsonProperty("token")
        val token: String
)