package com.itto.farmos.models

import android.os.Parcel
import android.os.Parcelable
import com.fasterxml.jackson.annotation.JsonProperty

data class User(
        @JsonProperty("url")
        val url: String,

        @JsonProperty("username")
        val username: String,

        @JsonProperty("last_name")
        val lastName: String,

        @JsonProperty("first_name")
        val firstName: String,

        @JsonProperty("email")
        val email: String,

        @JsonProperty("id")
        val id: Long,

        @JsonProperty("last_update")
        val last_update: Update

): Parcelable {
        constructor(parcel: Parcel) : this(
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readLong(),
                parcel.readParcelable(Update::class.java.classLoader)) {
        }

        override fun writeToParcel(parcel: Parcel, flags: Int) {
                parcel.writeString(url)
                parcel.writeString(username)
                parcel.writeString(lastName)
                parcel.writeString(firstName)
                parcel.writeString(email)
                parcel.writeLong(id)
                parcel.writeParcelable(last_update, flags)
        }

        override fun describeContents(): Int {
                return 0
        }

        companion object CREATOR : Parcelable.Creator<User> {
                override fun createFromParcel(parcel: Parcel): User {
                        return User(parcel)
                }

                override fun newArray(size: Int): Array<User?> {
                        return arrayOfNulls(size)
                }
        }
}