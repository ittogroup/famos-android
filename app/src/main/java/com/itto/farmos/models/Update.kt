package com.itto.farmos.models


import android.os.Parcel
import android.os.Parcelable
import com.fasterxml.jackson.annotation.JsonFormat
import com.fasterxml.jackson.annotation.JsonProperty
import java.util.*

data class Update(


        @JsonProperty("clients")
        val clients: Boolean,

        @JsonProperty("varieties")
        val varieties: Boolean,

        @JsonProperty("packaging")
        val packaging: Boolean,

        @JsonProperty("date")
        val date: String?

) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readByte() != 0.toByte(),
            parcel.readByte() != 0.toByte(),
            parcel.readByte() != 0.toByte(),
            parcel.readString()) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeByte(if (clients) 1 else 0)
        parcel.writeByte(if (varieties) 1 else 0)
        parcel.writeByte(if (packaging) 1 else 0)
        parcel.writeString(date)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Update> {
        override fun createFromParcel(parcel: Parcel): Update {
            return Update(parcel)
        }

        override fun newArray(size: Int): Array<Update?> {
            return arrayOfNulls(size)
        }
    }
}