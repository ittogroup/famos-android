package com.itto.farmos.models


import com.fasterxml.jackson.annotation.JsonProperty

data class BLCreateUpdate(
        @JsonProperty("pallets_ids")
        val pallets_ids: List<Long>,

        @JsonProperty("pallets_ids_remove")
        val pallets_ids_remove: List<Long>,

        @JsonProperty("bl_id")
        val bl_id: Long?,

        @JsonProperty("client_id")
        val client_id: Long

)