package com.itto.farmos.models

import android.os.Parcelable
import com.fasterxml.jackson.annotation.JsonProperty

data class Variety(
        @JsonProperty("id")
        var id: Long,

        @JsonProperty("name")
        var name: String,

        @JsonProperty("code")
        var code: String,

        @JsonProperty("color")
        var color: String,

        @JsonProperty("parcel")
        var parcel: Parcel

): Parcelable {
    constructor(pParcel: android.os.Parcel) : this(
            pParcel.readLong(),
            pParcel.readString(),
            pParcel.readString(),
            pParcel.readString(),
            pParcel.readParcelable(Parcel::class.java.classLoader) ){
    }

    override fun writeToParcel(pParcel: android.os.Parcel, flags: Int) {
        pParcel.writeLong(id)
        pParcel.writeString(name)
        pParcel.writeString(code)
        pParcel.writeString(color)
        pParcel.writeParcelable(parcel, flags)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Variety> {
        override fun createFromParcel(pParcel: android.os.Parcel): Variety {
            return Variety(pParcel)
        }

        override fun newArray(size: Int): Array<Variety?> {
            return arrayOfNulls(size)
        }
    }
}