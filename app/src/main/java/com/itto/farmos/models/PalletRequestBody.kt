package com.itto.farmos.models


import com.fasterxml.jackson.annotation.JsonFormat
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty
import java.util.*

@JsonInclude(JsonInclude.Include.NON_NULL)
data class PalletRequestBody(

        @JsonProperty("id")
        val id: Long?,

        @JsonProperty("destination_id")
        val destination_id: Long?,

        @JsonProperty("variety_id")
        val variety_id: Long?,

        @JsonProperty("processing_date")
        @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ssZ")
        val processing_date: Date?,

        @JsonProperty("entries")
        val entries: List<PalletEntry>,

        @JsonProperty("entries_to_remove")
        val entries_to_remove: List<Long>?,
        @JsonProperty("is_vrac")
        val vrac: Boolean?
)