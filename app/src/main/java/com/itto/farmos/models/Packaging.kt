package com.itto.farmos.models

import android.os.Parcel
import android.os.Parcelable
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonInclude.Include
import com.fasterxml.jackson.annotation.JsonProperty

data class Packaging(

        @JsonProperty("id")
        val id: Long,

        @JsonProperty("name")
        val name: String,

        @JsonProperty("weight")
        val weight: Double,

        @JsonProperty("packaging_type")
        val packaging_type: String?,

        @JsonProperty("is_pallet")
        var isPallet: Boolean? = false

): Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readLong(),
            parcel.readString(),
            parcel.readDouble(),
            parcel.readString(), parcel.readInt() == 1
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeLong(id)
        parcel.writeString(name)
        parcel.writeDouble(weight)
        parcel.writeString(packaging_type)
        parcel.writeInt( if (isPallet != null && isPallet == true) 1 else 0)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Packaging> {
        override fun createFromParcel(parcel: Parcel): Packaging {
            return Packaging(parcel)
        }

        override fun newArray(size: Int): Array<Packaging?> {
            return arrayOfNulls(size)
        }
    }
}