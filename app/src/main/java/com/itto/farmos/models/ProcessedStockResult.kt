package com.itto.farmos.models

import android.os.Parcel
import android.os.Parcelable
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty

@JsonInclude(JsonInclude.Include.NON_NULL)
data class ProcessedStockResult(

        @JsonProperty("count")
        val count: Int?,
        @JsonProperty("next")
        val next: String?,
        @JsonProperty("previous")
        val previous: String?,
        @JsonProperty("results")
        var results: List<ProcessedStock>?

): Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readString(),
            parcel.readString(),
            parcel.createTypedArrayList(ProcessedStock)) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeValue(count)
        parcel.writeString(next)
        parcel.writeString(previous)
        parcel.writeTypedList(results)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ProcessedStockResult> {
        override fun createFromParcel(parcel: Parcel): ProcessedStockResult {
            return ProcessedStockResult(parcel)
        }

        override fun newArray(size: Int): Array<ProcessedStockResult?> {
            return arrayOfNulls(size)
        }
    }

}