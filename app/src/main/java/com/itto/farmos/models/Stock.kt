package com.itto.farmos.models

import android.os.Parcelable
import com.fasterxml.jackson.annotation.JsonFormat
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty
import java.util.*

@JsonInclude(JsonInclude.Include.NON_NULL)
data class Stock(

        @JsonProperty("id")
        val id: Long?,
        @JsonProperty("variety")
        val variety: Variety?,
        @JsonProperty("parcel")
        val parcel: Parcel?,
        @JsonProperty("variety_id")
        var variety_id: Long?,
        @JsonProperty("parcel_id")
        var parcel_id: Long?,
        @JsonProperty("weight")
        var weight: Double?,
        @JsonProperty("date")
        @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ssZ")
        val date: Date?,
        @JsonProperty("temporary")
        var temporary: Boolean?,
        @JsonProperty("units")
        var units: Int?,
        @JsonProperty("age")
        val age: Int?,
        @JsonProperty("corporal")
        var corporal: String?,
        @JsonProperty("packaging")
        val packaging: Packaging?,
        @JsonProperty("packaging_id")
        var packaging_id: Long?,
        @JsonProperty("is_pallet")
        var isPallet: Boolean? = false
): Parcelable {
    constructor(pParcel: android.os.Parcel) : this(
            pParcel.readValue(Long::class.java.classLoader) as? Long,
            pParcel.readParcelable(Variety::class.java.classLoader),
            pParcel.readParcelable(Parcel::class.java.classLoader),
            pParcel.readValue(Long::class.java.classLoader) as? Long,
            pParcel.readValue(Long::class.java.classLoader) as? Long,
            pParcel.readValue(Double::class.java.classLoader) as? Double,
            Date((pParcel.readValue(Long::class.java.classLoader) as? Long)!!),
            pParcel.readValue(Boolean::class.java.classLoader) as? Boolean,
            pParcel.readValue(Int::class.java.classLoader) as? Int,
            pParcel.readValue(Int::class.java.classLoader) as? Int,
            pParcel.readValue(String::class.java.classLoader) as? String,
            pParcel.readParcelable(Packaging::class.java.classLoader),
            pParcel.readValue(Long::class.java.classLoader) as? Long,
            pParcel.readValue(Boolean::class.java.classLoader) as? Boolean) {
    }

    override fun writeToParcel(pParcel: android.os.Parcel, flags: Int) {
        pParcel.writeValue(id)
        pParcel.writeParcelable(variety, flags)
        pParcel.writeParcelable(parcel, flags)
        pParcel.writeValue(variety_id)
        pParcel.writeValue(parcel_id)
        pParcel.writeValue(weight)
        pParcel.writeValue(date!!.time)
        pParcel.writeValue(temporary)
        pParcel.writeValue(units)
        pParcel.writeValue(age)
        pParcel.writeValue(corporal)
        pParcel.writeParcelable(packaging, flags)
        pParcel.writeValue(packaging_id)
        pParcel.writeValue(isPallet)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Stock> {
        override fun createFromParcel(parcel: android.os.Parcel): Stock {
            return Stock(parcel)
        }

        override fun newArray(size: Int): Array<Stock?> {
            return arrayOfNulls(size)
        }
    }
}