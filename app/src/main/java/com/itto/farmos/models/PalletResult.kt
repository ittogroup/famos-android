package com.itto.farmos.models

import android.os.Parcel
import android.os.Parcelable
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty

@JsonInclude(JsonInclude.Include.NON_NULL)
data class PalletResult(

        @JsonProperty("count")
        val count: Int?,
        @JsonProperty("next")
        val next: String?,
        @JsonProperty("previous")
        val previous: String?,
        @JsonProperty("results")
        var results: List<Pallet>?

): Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readString(),
            parcel.readString(),
            parcel.createTypedArrayList(Pallet)) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeValue(count)
        parcel.writeString(next)
        parcel.writeString(previous)
        parcel.writeTypedList(results)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<PalletResult> {
        override fun createFromParcel(parcel: Parcel): PalletResult {
            return PalletResult(parcel)
        }

        override fun newArray(size: Int): Array<PalletResult?> {
            return arrayOfNulls(size)
        }
    }

}