package com.itto.farmos.models


import com.fasterxml.jackson.annotation.JsonProperty

data class ArchiveItems(
        @JsonProperty("archive_ids")
        val archive_ids: List<Int>,
        @JsonProperty("status")
        val status: String

)