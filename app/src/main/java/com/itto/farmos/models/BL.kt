package com.itto.farmos.models

import android.os.Parcel
import android.os.Parcelable
import com.fasterxml.jackson.annotation.JsonFormat
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty
import java.util.*

@JsonInclude(JsonInclude.Include.NON_NULL)
data class BL(
    @JsonProperty("id")
    val id: Long?,
    @JsonProperty("date")
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ssZ")
    val date: Date?,
    @JsonProperty("pallets")
    val pallets: List<Pallet>?,
    @JsonProperty("age")
    val age: Int?,
    @JsonProperty("client")
    val client: Client?
): Parcelable {
    constructor(pParcel: Parcel) : this(
            pParcel.readValue(Long::class.java.classLoader) as? Long,
            Date((pParcel.readValue(Long::class.java.classLoader) as? Long)!!),
            pParcel.createTypedArrayList(Pallet),
            pParcel.readValue(Int::class.java.classLoader) as? Int,
            pParcel.readParcelable(Client::class.java.classLoader)
            ) {
    }

    override fun writeToParcel(pParcel: Parcel, flags: Int) {
        pParcel.writeValue(id)
        pParcel.writeParcelable(client, flags)
        pParcel.writeValue(date!!.time)
        pParcel.writeTypedList(pallets)
        pParcel.writeValue(age)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<BL> {
        override fun createFromParcel(parcel: Parcel): BL {
            return BL(parcel)
        }

        override fun newArray(size: Int): Array<BL?> {
            return arrayOfNulls(size)
        }
    }

}