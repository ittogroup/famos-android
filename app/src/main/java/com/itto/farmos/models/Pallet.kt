package com.itto.farmos.models

import android.os.Parcel
import android.os.Parcelable
import com.fasterxml.jackson.annotation.JsonFormat
import com.fasterxml.jackson.annotation.JsonProperty
import java.util.*

data class Pallet(

        @JsonProperty("id")
        val id: Long,

        @JsonProperty("destination")
        val destination: Client?,

        @JsonProperty("variety")
        val variety: Variety,

        @JsonProperty("entries")
        val entries: List<PalletEntry>,

        @JsonProperty("date")
        @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ssZ")
        val date: Date?,

        @JsonProperty("processing_date")
        @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ssZ")
        val processing_date: Date?,

        @JsonProperty("age")
        val age: Int?,

        @JsonProperty("vrac")
        var vrac: Boolean?,

        @JsonProperty("pallet_packaging")
        var packaging: Packaging?

): Parcelable {
    constructor(pParcel: Parcel) : this(
            pParcel.readLong(),
            pParcel.readParcelable(Client::class.java.classLoader),
            pParcel.readParcelable(Variety::class.java.classLoader),
            pParcel.createTypedArrayList(PalletEntry),
            Date((pParcel.readValue(Long::class.java.classLoader) as? Long)!!),
            Date((pParcel.readValue(Long::class.java.classLoader) as? Long)!!),
            pParcel.readValue(Int::class.java.classLoader) as? Int,
            pParcel.readValue(Boolean::class.java.classLoader) as? Boolean,
            pParcel.readParcelable(Packaging::class.java.classLoader)
            ) {
    }

    override fun writeToParcel(pParcel: Parcel, flags: Int) {
        pParcel.writeLong(id)
        pParcel.writeParcelable(destination, flags)
        pParcel.writeParcelable(variety, flags)
        pParcel.writeTypedList(entries)
        pParcel.writeValue(date!!.time)
        if (processing_date == null) {
            pParcel.writeValue(date.time)
        }else{
            pParcel.writeValue(processing_date.time)

        }
        pParcel.writeValue(age)
        pParcel.writeValue(vrac)
        pParcel.writeParcelable(packaging, flags)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Pallet> {
        override fun createFromParcel(parcel: Parcel): Pallet {
            return Pallet(parcel)
        }

        override fun newArray(size: Int): Array<Pallet?> {
            return arrayOfNulls(size)
        }
    }
}