package com.itto.farmos.models

import android.os.Parcelable
import com.fasterxml.jackson.annotation.JsonProperty

data class Parcel(
        @JsonProperty("id")
        var id: Long,

        @JsonProperty("name")
        var name: String,

        @JsonProperty("product")
        var product: Product,

        @JsonProperty("code")
        var code: Long


) : Parcelable {
    constructor(parcel: android.os.Parcel) : this(
            parcel.readLong(),
            parcel.readString(),
            parcel.readParcelable(Product::class.java.classLoader),
            parcel.readLong()) {
    }

    override fun writeToParcel(parcel: android.os.Parcel, flags: Int) {
        parcel.writeLong(id)
        parcel.writeString(name)
        parcel.writeParcelable(product, flags)
        parcel.writeLong(code)

    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Parcel> {
        override fun createFromParcel(parcel: android.os.Parcel): Parcel {
            return Parcel(parcel)
        }

        override fun newArray(size: Int): Array<Parcel?> {
            return arrayOfNulls(size)
        }
    }

}