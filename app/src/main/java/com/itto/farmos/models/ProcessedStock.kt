package com.itto.farmos.models

import android.os.Parcel
import android.os.Parcelable
import com.fasterxml.jackson.annotation.JsonFormat
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty
import java.util.*

@JsonInclude(JsonInclude.Include.NON_NULL)
data class ProcessedStock(
    @JsonProperty("id")
    val id: Long?,
    @JsonProperty("date")
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ssZ")
    val date: Date?,
    @JsonProperty("stock")
    val stock: Stock?,
    @JsonProperty("stock_id")
    val stock_id: Long?,
    @JsonProperty("age")
    val age: Int?
): Parcelable {
    constructor(pParcel: Parcel) : this(
            pParcel.readValue(Long::class.java.classLoader) as? Long,
            Date((pParcel.readValue(Long::class.java.classLoader) as? Long)!!),
            pParcel.readParcelable(Stock::class.java.classLoader),
            pParcel.readValue(Long::class.java.classLoader) as? Long,
            pParcel.readValue(Int::class.java.classLoader) as? Int) {
    }

    override fun writeToParcel(pParcel: Parcel, flags: Int) {
        pParcel.writeValue(id)
        pParcel.writeValue(date!!.time)
        pParcel.writeParcelable(stock, flags)
        pParcel.writeValue(stock_id)
        pParcel.writeValue(age)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ProcessedStock> {
        override fun createFromParcel(parcel: Parcel): ProcessedStock {
            return ProcessedStock(parcel)
        }

        override fun newArray(size: Int): Array<ProcessedStock?> {
            return arrayOfNulls(size)
        }
    }

}