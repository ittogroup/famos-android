package com.itto.farmos.models


import android.os.Parcel
import android.os.Parcelable
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty

@JsonInclude(JsonInclude.Include.NON_NULL)
data class PalletEntry(

        @JsonProperty("id")
        val id: Long?,

        @JsonProperty("units")
        val units: Int,

        @JsonProperty("calibre")
        val calibre: Int,

        @JsonProperty("weight")
        var weight: Double?,

        @JsonProperty("weight_gross")
        var weight_gross: Double?,

        @JsonProperty("packaging")
        val packaging: Packaging?,

        @JsonProperty("packaging_id")
        var packaging_id: Long?

): Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readValue(Long::class.java.classLoader) as? Long,
            parcel.readInt(),
            parcel.readInt(),
            parcel.readValue(Double::class.java.classLoader) as? Double,
            parcel.readValue(Double::class.java.classLoader) as? Double,
            parcel.readParcelable(Packaging::class.java.classLoader),
            parcel.readValue(Long::class.java.classLoader) as? Long) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeValue(id)
        parcel.writeInt(units)
        parcel.writeInt(calibre)
        parcel.writeValue(weight)
        parcel.writeValue(weight_gross)
        parcel.writeParcelable(packaging, flags)
        parcel.writeValue(packaging_id)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<PalletEntry> {
        override fun createFromParcel(parcel: Parcel): PalletEntry {
            return PalletEntry(parcel)
        }

        override fun newArray(size: Int): Array<PalletEntry?> {
            return arrayOfNulls(size)
        }
    }
}