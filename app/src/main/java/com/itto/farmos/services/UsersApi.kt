package com.itto.farmos.services

import android.text.Editable
import com.itto.farmos.config.Constants
import com.itto.farmos.models.Token
import com.itto.farmos.models.User
import io.reactivex.Observable
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.jackson.JacksonConverterFactory
import retrofit2.http.*


interface UsersApi {

    @FormUrlEncoded
    @POST("/api/login/")
    fun login(@Field("username") pUserName: Editable, @Field("password") pPassword: Editable): Observable<Token>

    @GET("/api/users/{user_id}/")
    fun user(@Path("user_id") userId: String, @Header("Authorization") pAuthorization: String): Observable<User>


    @GET("/api/users/{user_id}/")
    fun userSync(@Path("user_id") userId: String, @Header("Authorization") pAuthorization: String): Call<User>

    @GET("/api/logout/")
    fun logout(@Header("Authorization") pAuthorization: String): Observable<User>


    companion object Factory {

        fun create(): UsersApi {
            val retrofit = Retrofit.Builder()
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(JacksonConverterFactory.create())
                    .baseUrl(Constants.BASE_URL)
                    .build()

            return retrofit.create(UsersApi::class.java)
        }
    }
}

