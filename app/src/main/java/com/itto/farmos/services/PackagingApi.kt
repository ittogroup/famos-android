package com.itto.farmos.services

import com.itto.farmos.config.Constants
import com.itto.farmos.models.City
import com.itto.farmos.models.Packaging
import io.reactivex.Observable
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.jackson.JacksonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Header

interface PackagingApi {

    @GET("/api/packaging/")
    fun packaging(@Header("Authorization") pAuthorization: String): Observable<List<Packaging>>

    companion object Factory {

        fun create(): PackagingApi {
            val retrofit = Retrofit.Builder()
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(JacksonConverterFactory.create())
                    .baseUrl(Constants.BASE_URL)
                    .build()

            return retrofit.create(PackagingApi::class.java)
        }
    }
}