package com.itto.farmos.services

import com.itto.farmos.config.Constants
import com.itto.farmos.models.*
import io.reactivex.Observable
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.jackson.JacksonConverterFactory
import retrofit2.http.Body
import retrofit2.http.Header
import retrofit2.http.Headers
import retrofit2.http.POST

interface FcmTokenApi {

    @POST("/api/fcm/token")
    @Headers("Content-Type: application/json")
    fun sendFcmToken(@Header("Authorization") pAuthorization: String, @Body fcmToken: FcmToken): Observable<FcmTokenResponse>

    companion object Factory {

        fun create(): FcmTokenApi {
            val retrofit = Retrofit.Builder()
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(JacksonConverterFactory.create())
                    .baseUrl(Constants.BASE_URL)
                    .build()

            return retrofit.create(FcmTokenApi::class.java)
        }
    }
}