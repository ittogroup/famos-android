package com.itto.farmos.services

import com.itto.farmos.config.Constants
import com.itto.farmos.models.*
import io.reactivex.Observable
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.jackson.JacksonConverterFactory
import retrofit2.http.*

interface BLApi {

    @GET("/api/bl/")
    fun bls(@Header("Authorization") pAuthorization: String, @Query("limit") limit: Int, @Query("offset") offset: Int): Observable<BLResult>

    @GET("/api/bl/PalletByBL")
    fun blPallets(@Header("Authorization") pAuthorization: String, @Query("bl_id") bl_id: Long?): Observable<List<Pallet>>

    @POST("/api/bl/create_update/")
    @Headers("Content-Type: application/json")
    fun addBL(@Header("Authorization") pAuthorization: String, @Body bl: BLCreateUpdate): Observable<BLCreateUpdate>

    @PUT("/api/bl/archive/")
    @Headers("Content-Type: application/json")
    fun archiveBL(@Header("Authorization") pAuthorization: String, @Body archiveItems: ArchiveItems): Observable<ArchiveItems>

    @DELETE("/api/bl/{pallet_id}/")
    fun deleteBL(@Path("pallet_id") pallet_id: Long, @Header("Authorization") pAuthorization: String): Observable<Pallet>

    companion object Factory {

        fun create(): BLApi {
            val retrofit = Retrofit.Builder()
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(JacksonConverterFactory.create())
                    .baseUrl(Constants.BASE_URL)
                    .build()

            return retrofit.create(BLApi::class.java)
        }
    }
}