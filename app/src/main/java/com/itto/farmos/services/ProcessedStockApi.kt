package com.itto.farmos.services

import com.itto.farmos.config.Constants
import com.itto.farmos.models.*
import io.reactivex.Observable
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.jackson.JacksonConverterFactory
import retrofit2.http.*

interface ProcessedStockApi {

    @GET("/api/processed_stocks/")
    fun processed_stocks(@Header("Authorization") pAuthorization: String, @Query("limit") limit: Int, @Query("offset") offset: Int): Observable<ProcessedStockResult>

    @DELETE("/api/processed_stocks/{processed_stock_id}/")
    fun deleteProcessedStock(@Path("processed_stock_id") processedStockId: Long, @Header("Authorization") pAuthorization: String): Observable<ProcessedStock>

    @PUT("/api/processed_stocks/archive/")
    @Headers("Content-Type: application/json")
    fun archiveProcessedStock(@Header("Authorization") pAuthorization: String, @Body archiveItems: ArchiveItems): Observable<ArchiveItems>

    @POST("/api/processed_stocks/")
    @Headers("Content-Type: application/json")
    fun processStock(@Header("Authorization") pAuthorization: String, @Body processedStock: ProcessedStock): Observable<ProcessedStock>

    companion object Factory {

        fun create(): ProcessedStockApi {
            val retrofit = Retrofit.Builder()
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(JacksonConverterFactory.create())
                    .baseUrl(Constants.BASE_URL)
                    .build()

            return retrofit.create(ProcessedStockApi::class.java)
        }
    }
}