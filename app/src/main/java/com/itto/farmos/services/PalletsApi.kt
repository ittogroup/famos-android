package com.itto.farmos.services

import com.itto.farmos.config.Constants
import com.itto.farmos.models.*
import io.reactivex.Observable
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.jackson.JacksonConverterFactory
import retrofit2.http.*

interface PalletsApi {

    @GET("/api/pallets/")
    fun pallets(@Header("Authorization") pAuthorization: String, @Query("limit") limit: Int, @Query("offset") offset: Int): Observable<PalletResult>

    @POST("/api/pallets/save_pallet/")
    @Headers("Content-Type: application/json")
    fun addPallet(@Header("Authorization") pAuthorization: String, @Body pallet: PalletRequestBody): Observable<Pallet>

    @PUT("/api/pallets/update_pallet/")
    @Headers("Content-Type: application/json")
    fun updatePallet(@Header("Authorization") pAuthorization: String, @Body pallet: PalletRequestBody): Observable<Pallet>

    @PUT("/api/pallets/archive/")
    @Headers("Content-Type: application/json")
    fun archivePallet(@Header("Authorization") pAuthorization: String, @Body archiveItems: ArchiveItems): Observable<ArchiveItems>

    @DELETE("/api/pallets/{pallet_id}/")
    fun deletePallet(@Path("pallet_id") pallet_id: Long, @Header("Authorization") pAuthorization: String): Observable<Pallet>

    @GET("/api/pallets/{pallet_id}/")
    fun getPallet(@Path("pallet_id") pallet_id: Long, @Header("Authorization") pAuthorization: String): Observable<Pallet>

    companion object Factory {

        fun create(): PalletsApi {
            val logging = HttpLoggingInterceptor()
            logging.level = HttpLoggingInterceptor.Level.BODY

            val httpClient = OkHttpClient.Builder()
            httpClient.addInterceptor(logging)

            val retrofit = Retrofit.Builder()
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(JacksonConverterFactory.create())
                    .baseUrl(Constants.BASE_URL)
                    .client(httpClient.build())
                    .build()

            return retrofit.create(PalletsApi::class.java)
        }
    }
}