package com.itto.farmos.services

import com.itto.farmos.config.Constants
import com.itto.farmos.models.ArchiveItems
import com.itto.farmos.models.Stock
import com.itto.farmos.models.StockResult
import io.reactivex.Observable
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.jackson.JacksonConverterFactory
import retrofit2.http.*
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor


interface StockApi {

    @GET("/api/stocks/")
    fun stocks(@Header("Authorization") pAuthorization: String,
               @Query("limit") limit: Int,
               @Query("offset") offset: Int): Observable<StockResult>

    @GET("/api/stocks/searchstock/")
    fun searchStocks(@Header("Authorization") pAuthorization: String,
                     @Query("limit") limit: Int,
                     @Query("offset") offset: Int,
                     @Query("pallet_id") palletId: String,
                     @Query("variety_id") varietyId: String,
                     @Query("units") units: String,
                     @Query("weight") weight: String,
                     @Query("corporal") corporal: String): Observable<StockResult>

    @POST("/api/stocks/")
    @Headers("Content-Type: application/json")
    fun addStock(@Header("Authorization") pAuthorization: String,
                 @Body stock: Stock): Observable<Stock>

    @PUT("/api/stocks/{stock_id}/")
    @Headers("Content-Type: application/json")
    fun updateStock(@Header("Authorization") pAuthorization: String,
                    @Path("stock_id") stock_id: Long,
                    @Body stock: Stock): Observable<Stock>

    @PUT("/api/stocks/archive/")
    @Headers("Content-Type: application/json")
    fun archiveStock(@Header("Authorization") pAuthorization: String, @Body archiveItems: ArchiveItems): Observable<ArchiveItems>

    @DELETE("/api/stocks/{stock_id}/")
    fun deleteStock(@Path("stock_id") stock_id: Long, @Header("Authorization") pAuthorization: String): Observable<Stock>

    @GET("/api/stocks/{stock_id}/")
    fun getStock(@Path("stock_id") stock_id: Long, @Header("Authorization") pAuthorization: String): Observable<Stock>

    companion object Factory {

        fun create(): StockApi {
            val logging = HttpLoggingInterceptor()
            // set your desired log level
            logging.level = HttpLoggingInterceptor.Level.BODY

            val httpClient = OkHttpClient.Builder()
            // add your other interceptors …

            // add logging as last interceptor
            httpClient.addInterceptor(logging)
            val retrofit = Retrofit.Builder()
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(JacksonConverterFactory.create())
                    .client(httpClient.build())
                    .baseUrl(Constants.BASE_URL)
                    .build()

            return retrofit.create(StockApi::class.java)
        }
    }

}