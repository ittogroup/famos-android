package com.itto.farmos.activities.pallet

import android.Manifest
import android.app.ProgressDialog
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.AsyncTask
import android.os.Build
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.text.Editable
import android.text.TextWatcher
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.AdapterView

import com.itto.farmos.R
import com.itto.farmos.adapters.ClientAdapter
import com.itto.farmos.config.Constants
import com.itto.farmos.fragments.PalletFragment
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_detail_pallet.*
import kotlinx.android.synthetic.main.view_entry_layout.view.*
import org.jetbrains.anko.indeterminateProgressDialog
import android.widget.LinearLayout
import android.widget.Toast
import com.citizen.jpos.command.CPCLConst
import com.citizen.jpos.printer.CPCLPrinter
import com.citizen.port.android.BluetoothPort
import com.citizen.request.android.RequestHandler
import com.itto.farmos.adapters.PackagingAdapter
import com.itto.farmos.adapters.VarietyAdapter
import com.itto.farmos.models.*
import com.itto.farmos.printer.assist.AlertView
import com.itto.farmos.services.*
import com.itto.farmos.tools.LoaderData
import org.jetbrains.anko.alert
import org.jetbrains.anko.noButton
import org.jetbrains.anko.yesButton
import java.io.IOException
import java.io.UnsupportedEncodingException
import java.text.*
import java.time.format.DateTimeFormatter
import java.util.*


class DetailPalletActivity : AppCompatActivity() {

    private var selectedVarietyId: Long? = null
    private var selectedDestinationId: Long? = null
    private var pallet: Pallet? = null
    private var mMenu: Menu? = null
    private var entriesIdToRemove = ArrayList<Long>()
    private var context: Context? = null
    private var hThread: Thread? = null
    private var connectionTask: connTask? = null
    private var remoteDevices: Vector<BluetoothDevice>? = null

    private var selectedPackaging: HashMap<Int, Packaging>? = null
    private var selectedPackagingIndex: Int = 0

    private var bluetoothPort: BluetoothPort? = null
    private var mBluetoothAdapter: BluetoothAdapter? = null
    private val REQUEST_ENABLE_BT = 2

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_pallet)
        context = this
        selectedPackaging = HashMap()

        supportActionBar!!.setHomeButtonEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        pallet = intent.getParcelableExtra<Pallet>(PalletFragment.DETAIL_PALLET)
        var isPrint = false

        if (pallet == null) {
            pallet = intent.getParcelableExtra<Pallet>(AddPalletActivity.PRINT_PALLET)
            isPrint = true
        }
        val variety = pallet!!.variety
        selectedVarietyId = variety!!.id
        varietyTextView.setText(variety!!.name)
        vracCheckBox.isChecked = pallet!!.vrac!!
        val destination = pallet!!.destination
        if (destination != null) {
            selectedDestinationId = destination!!.id
            destinationSpinner.setText(destination!!.name)
        }

        title = getString(R.string.pallet) + pallet!!.id
        if (pallet!!.processing_date != null) {

            var formatter = SimpleDateFormat(resources.getString(R.string.dateFormat))
            var formattedDate = formatter.format(pallet!!.processing_date)
            processingDate.setText(formattedDate)
        }
        val varietyAdapter = VarietyAdapter(this, LoaderData.instance.varieties)
        varietyTextView.setAdapter(varietyAdapter)
        varietyTextView.threshold = 0

        val clientAdapter = ClientAdapter(this, null)
        destinationSpinner.setAdapter(clientAdapter)

        val prefs = getSharedPreferences(Constants.SHARED_PREFS, 0)
        val token = prefs.getString(Constants.USER_TOKEN, "")

        val progress = indeterminateProgressDialog((context as DetailPalletActivity).resources.getString(R.string.message_loading))
        progress.setCancelable(false)
        progress.setCanceledOnTouchOutside(false)
        progress.show()


        varietyTextView.setOnItemClickListener { _: AdapterView<*>, _: View, _: Int, id: Long ->
            selectedVarietyId = id
        }

        val clientsServices = ClientsApi.create()

        clientsServices.getClients("token  $token")
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({ clients ->

                    clientAdapter.clients = clients
                    clientAdapter.notifyDataSetChanged()

                    progress.dismiss()


                }, { error ->
                    error.printStackTrace()
                    progress.dismiss()
                })

        destinationSpinner.setOnItemClickListener { _: AdapterView<*>, _: View, _: Int, id: Long ->
            selectedDestinationId = id
        }

        vracCheckBox.setOnCheckedChangeListener { buttonView, isChecked ->
            for (i in 0..(palletEntries.childCount - 1)) {
                val entryView = palletEntries.getChildAt(i)
                if (isChecked) {
                    entryView.calibreText.visibility = View.GONE
                    entryView.calibreLabel.visibility = View.GONE
                } else {
                    entryView.calibreText.visibility = View.VISIBLE
                    entryView.calibreLabel.visibility = View.VISIBLE
                }
            }

        }

        addEntryButton.setOnClickListener {
            addEntry(null)
        }

        updateEntriesTagId()
        clearBtDevData()
        bluetoothSetup()
        if (mBluetoothAdapter != null) {
            if (bluetoothPort != null && !bluetoothPort!!.isConnected) {
                var pairedDevice: BluetoothDevice
                val iter = mBluetoothAdapter!!.getBondedDevices().iterator()
                while (iter.hasNext()) {
                    pairedDevice = iter.next()
                    remoteDevices!!.add(pairedDevice)
                }
                if (remoteDevices == null || remoteDevices!!.size == 0) {
                    return
                }
                pairedDevice = remoteDevices!![0]
                if (connectionTask != null && connectionTask!!.getStatus() == AsyncTask.Status.RUNNING) {
                    connectionTask!!.cancel(true)
                    if (!connectionTask!!.isCancelled())
                        connectionTask!!.cancel(true)
                    connectionTask = null
                }
                connectionTask = connTask()
                connectionTask!!.execute(pairedDevice)
            }
        }
        if (isPrint) {
            alert("Do you want to print (Pallet " + pallet!!.id.toString() + ")") {
                yesButton {
                    print()
                }
                noButton {}
            }.show()
        }
    }

    // clear device data used list.
    private fun clearBtDevData() {
        remoteDevices = Vector()
    }


    // Set up Bluetooth.
    private fun bluetoothSetup() {
        // Initialize

        bluetoothPort = BluetoothPort.getInstance()
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter()

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val CODE = 5 // app defined constant used for onRequestPermissionsResult

            val permissionsToRequest = arrayOf(Manifest.permission.BLUETOOTH_ADMIN, Manifest.permission.BLUETOOTH, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION)

            var allPermissionsGranted = true

            for (permission in permissionsToRequest) {
                allPermissionsGranted = allPermissionsGranted && ContextCompat.checkSelfPermission(this, permission) == PackageManager.PERMISSION_GRANTED
            }

            if (!allPermissionsGranted) {
                ActivityCompat.requestPermissions(this, permissionsToRequest, CODE)
            }
        }
        if (mBluetoothAdapter == null) {
            // Device does not support Bluetooth
            return
        }
        if (!(mBluetoothAdapter?.isEnabled()!!)) {
            val enableBtIntent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT)
        }
    }

    internal inner class connTask : AsyncTask<BluetoothDevice, Void, Int>() {
        private var dialog = ProgressDialog(this@DetailPalletActivity)

        override fun onPreExecute() {
            dialog.setTitle(resources.getString(R.string.bt_tab))
            dialog.setMessage(resources.getString(R.string.connecting_msg))
            dialog.show()
            super.onPreExecute()
        }

        override fun doInBackground(vararg params: BluetoothDevice): Int? {
            var retVal: Int? = null
            try {
                bluetoothPort!!.connect(params[0])
                retVal = Integer.valueOf(0)
            } catch (e: IOException) {
                retVal = Integer.valueOf(-1)
            }

            return retVal
        }

        override fun onPostExecute(result: Int?) {
            if (result!!.toInt() == 0)
            // Connection success.
            {
                val rh = RequestHandler()
                hThread = Thread(rh)
                hThread!!.start()

                if (dialog.isShowing)
                    dialog.dismiss()
                val toast = Toast.makeText(context, resources.getString(R.string.bt_conn_msg), Toast.LENGTH_SHORT)
                toast.show()

            } else
            // Connection failed.
            {
                if (dialog.isShowing)
                    dialog.dismiss()
                AlertView.showAlert(resources.getString(R.string.bt_conn_fail_msg),
                        resources.getString(R.string.dev_check_msg), context)
            }
            super.onPostExecute(result)
        }
    }


    private fun addEntry(palletEntry: PalletEntry?) {
        val entryView = layoutInflater.inflate(R.layout.view_entry_layout, null)
        val longClickListener = { _: View ->

            if (entryView.removeButton.visibility == View.GONE && palletEntries.childCount > 1) {
                entryView.setBackgroundColor(resources.getColor(R.color.colorPrimaryDark))
                entryView.removeButton.visibility = View.VISIBLE
                entryView.calibreText.isEnabled = false
                entryView.unitsText.isEnabled = false
                entryView.weightText.isEnabled = false
                entryView.weightGrossText.isEnabled = false
            } else {
                entryView.removeButton.visibility = View.GONE
                entryView.setBackgroundColor(resources.getColor(R.color.whiteSmoke))
                entryView.calibreText.isEnabled = true
                entryView.unitsText.isEnabled = true
                entryView.weightText.isEnabled = true
                entryView.weightGrossText.isEnabled = true


            }

            true
        }
        entryView.packagingTextView.tag = selectedPackagingIndex
        if (palletEntry != null) {
            entryView.tag = palletEntry.id
            entryView.calibreText.setText(palletEntry!!.calibre!!.toString())
            entryView.unitsText.setText(palletEntry!!.units!!.toString())
            entryView.weightGrossText.setText(palletEntry!!.weight!!.toString())
//            entryView.weightGrossText.setText(palletEntry!!.weight_gross!!.toString())

            if (palletEntry.packaging != null) {
                entryView.packagingTextView.setText(palletEntry.packaging!!.name)

                selectedPackaging!![entryView.packagingTextView.tag as Int] = palletEntry.packaging


            }
            updateWeight(entryView)
        }

        entryView.calibreText.setOnLongClickListener(longClickListener)
        entryView.unitsText.setOnLongClickListener(longClickListener)
        entryView.weightGrossText.setOnLongClickListener(longClickListener)
        entryView.setOnLongClickListener(longClickListener)

        val packagingAdapter = PackagingAdapter(this, LoaderData.instance.packaging)
        packagingAdapter.packagingType = "pf"
        entryView.packagingTextView.setAdapter(packagingAdapter)
        entryView.packagingTextView.threshold = 0

        entryView.packagingTextView.setOnItemClickListener { _: AdapterView<*>, _: View, _: Int, id: Long ->
            val selectedPack = packagingAdapter.packaging!!.filter {
                it.id == id
            }[0]
            selectedPackaging!![entryView.packagingTextView.tag as Int] = selectedPack
            updateWeight(entryView)
        }
        entryView.weightGrossText.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                updateWeight(entryView)
            }

        })
        entryView.unitsText.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                updateWeight(entryView)
            }
        })

        selectedPackagingIndex++

        palletEntries.addView(entryView)
        var isVrac = vracCheckBox.isChecked

        if (isVrac) {
            entryView.calibreText.visibility = View.GONE
            entryView.calibreLabel.visibility = View.GONE
            entryView.calibreText.setText("0")
        } else {
            entryView.calibreText.visibility = View.VISIBLE
            entryView.calibreLabel.visibility = View.VISIBLE

        }
        entryView.removeButton.setOnClickListener {
            if (palletEntries.childCount > 1) {
                entriesIdToRemove.add(entryView.tag as Long)
                palletEntries.removeView(entryView)
            }
        }
    }

    private fun updateWeight(entryView: View) {
        val selectedPack = selectedPackaging!![entryView.packagingTextView.tag as Int]
        if (selectedPack == null && entryView.weightGrossText.text.toString() == "" && entryView.unitsText.text.toString() == "") {
            entryView.weightText.text = "--"
        } else if (selectedPack == null && entryView.weightGrossText.text.toString() != "") {
            entryView.weightText.text = entryView.weightGrossText.text.toString()
        } else if (entryView.weightGrossText.text.toString() == "" || entryView.unitsText.text.toString() == "") {
            entryView.weightText.text = "--"
        } else {
            var weightStr = entryView.weightGrossText.text.toString()
            val nf = NumberFormat.getInstance(Locale.ENGLISH)
            val weight = nf.parse(weightStr.replace(",", ".")).toDouble()
            val units = entryView.unitsText.text.toString()

            val unit = units.toInt()
            val netWeight = (weight - (unit * selectedPack!!.weight) - 18)

            entryView.weightText.text = DecimalFormat("#.#", DecimalFormatSymbols(Locale.ENGLISH)).format(netWeight)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.detail_pallet_drawer, menu)
        mMenu = menu
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item!!.itemId) {
            android.R.id.home -> {
                finish()
                return true
            }
            R.id.delete -> {
                alert("Are you sure you want to delete (Pallet " + pallet!!.id.toString() + ")") {
                    yesButton {
                        deletePallet(item)
                    }
                    noButton {}
                }.show()
                return true
            }
            R.id.print -> {
                print()
                return true
            }
            R.id.printPackage -> {
                val nbrPackage = pallet!!.entries.sumBy { entrey -> entrey.units }
                alert("Printing " + nbrPackage + " package tickets") {
                    yesButton {
                        printPackageTecket()
                    }
                    noButton {}
                }.show()
                return true
            }
            R.id.update -> {
                updatePallet(item)
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun print() {
        val cpclPrinter = CPCLPrinter()

        cpclPrinter.setForm(0, 1, 1, 800, 1)
        val sharedPref = this.getSharedPreferences(Constants.SHARED_PREFS, 0)
        val selectedPaperType = sharedPref.getInt(Constants.SELECTED_PAPER_TYPE, 0)

        cpclPrinter.setMedia(selectedPaperType)
        if (selectedPaperType == 2) {
            cpclPrinter.printBox(0, 0, 570, 740, 1)
        }
        cpclPrinter.printCPCL2DBarCode(0, CPCLConst.CMP_CPCL_BCS_QRCODE, 50, 20, 10, 0, 1, 0, "Palette-" + pallet!!.id.toString())

        cpclPrinter.printCPCLText(0, 5, 2, 265, 20, "Domaine Itto", 0)
        val dateFormat = SimpleDateFormat("MM/dd/yyyy")
        val formattedDate = dateFormat.format(pallet!!.processing_date)

        cpclPrinter.printCPCLText(0, 0, 3, 265, 60, "%s :".format("Date"), 0)
        cpclPrinter.printCPCLText(0, 0, 3, 265, 90, "%s".format(formattedDate), 0)
        cpclPrinter.printCPCLText(0, 0, 3, 265, 120, "%s :".format("Produit"), 0)
        cpclPrinter.printCPCLText(0, 0, 3, 265, 150, "%s / %s".format(pallet!!.variety.parcel.product.name, pallet!!.variety.code), 0)
        cpclPrinter.printCPCLText(0, 0, 3, 265, 180, "%s :".format("Destination"), 0)
        if (pallet!!.destination != null) {
            cpclPrinter.printCPCLText(0, 0, 3, 265, 210, "%s".format(pallet!!.destination!!.name), 0)
            cpclPrinter.printCPCLText(0, 0, 3, 265, 240, "%s".format(pallet!!.destination!!.city!!.name), 0)
        }
        if (pallet!!.vrac!!) {
            cpclPrinter.printCPCLText(0, 3, 2, 40, 240, ">> VRAC", 0)

        }
        var productInfoLine1 = ""
        var line = 360
//        cpclPrinter.printCPCLText(0, 0, 3, 40, 230, "%s :".format("Pallet Id", pallet!!.id.toString()), 0)
        cpclPrinter.printCPCLText(0, 0, 3, 40, 270, "%s %s".format("Palette",pallet!!.id.toString()), 0)
        line += 35
        var totalNetWeight = 0.0
        var totalUnit = 0
        for (palletEntry: PalletEntry in pallet!!.entries) {
            val netWeight = if (palletEntry.packaging != null && palletEntry.units != null && palletEntry.weight != null) {
                (palletEntry.weight as Double - (palletEntry.units as Int * palletEntry.packaging!!.weight) - 18)
            } else {
                palletEntry.weight
            }
            totalNetWeight += netWeight!!
            totalUnit += palletEntry.units
            val weightStr = DecimalFormat("#.#", DecimalFormatSymbols(Locale.ENGLISH)).format(netWeight)

            productInfoLine1 = "C %s; U %s ; Poids Net %s kg ".format(palletEntry.calibre, palletEntry.units, weightStr)
            cpclPrinter.printCPCLText(0, 0, 2, 40, line, productInfoLine1, 0)

            line += 33

        }
        cpclPrinter.printCPCLText(0, 0, 3, 40, 360, "Auto. ONSSA : FLF.14.40.19", 0)

        val weightStr = DecimalFormat("#.#", DecimalFormatSymbols(Locale.ENGLISH)).format(totalNetWeight)

        cpclPrinter.printCPCLText(0, 0, 3, 40, 300, "Total Colis : %s".format(totalUnit.toString()), 0)
        cpclPrinter.printCPCLText(0, 0, 3, 40, 330, "Total Poids Net : %s  kg".format(weightStr.toString()), 0)


        try {
            cpclPrinter.printForm()
        } catch (e: UnsupportedEncodingException) {
            e.printStackTrace()
        }
    }


    private fun printPackageTecket() {
        var indexPackge = 0
        for (palletEntry: PalletEntry in pallet!!.entries) {
            for (unitIndex in 0 until palletEntry.units) {
                indexPackge++
                val netWeight = if (palletEntry.packaging != null && palletEntry.units != null && palletEntry.weight != null) {
                    (palletEntry.weight as Double - (palletEntry.units as Int * palletEntry.packaging!!.weight) - 18)
                } else {
                    palletEntry.weight
                }
                val packageNetWeight = netWeight!! / palletEntry.units
                val weightStr = DecimalFormat("#.##", DecimalFormatSymbols(Locale.ENGLISH)).format(packageNetWeight)

                val cpclPrinter = CPCLPrinter()

                cpclPrinter.setForm(0, 1, 1, 400, 1)
                val sharedPref = this.getSharedPreferences(Constants.SHARED_PREFS, 0)
                val selectedPaperType = sharedPref.getInt(Constants.SELECTED_PAPER_TYPE, 0)

                cpclPrinter.setMedia(selectedPaperType)


                cpclPrinter.printCPCLText(0, 0, 3, 40, 40, "Producteur : Domaine Itto SARL", 0)
                cpclPrinter.printCPCLText(0, 0, 3, 40, 70, "Origine : Maroc", 0)

                val dateFormat = SimpleDateFormat("MM/dd/yyyy")
                val formattedDate = dateFormat.format(pallet!!.date)
                val idColis = if (indexPackge < 10){
                    pallet!!.id.toString() + "-00" + indexPackge.toString()
                }else if(indexPackge in 10..99){
                    pallet!!.id.toString() + "-0" + indexPackge.toString()

                }else{
                    pallet!!.id.toString() +"-"+ indexPackge.toString()

                }
                cpclPrinter.printCPCLText(0, 0, 3, 40, 100, "%s : %s".format("Palette", pallet!!.id.toString()), 0)
                cpclPrinter.printCPCLText(0, 0, 3, 40, 130, "%s : %s".format("Date", formattedDate), 0)
                cpclPrinter.printCPCLText(0, 0, 3, 40, 160, "%s : %s".format("Code", pallet!!.variety.code), 0)
                cpclPrinter.printCPCLText(0, 0, 3, 40, 190, "%s : %s".format("Type", pallet!!.variety.parcel.product.name), 0)
                cpclPrinter.printCPCLText(0, 0, 3, 40, 220, "%s : %s".format("Colis", idColis), 0)
                cpclPrinter.printCPCLText(0, 0, 3, 40, 250, "%s : %s".format("Calibre", palletEntry.calibre.toString()), 0)
                cpclPrinter.printCPCLText(0, 0, 3, 40, 280, "%s : %s kg".format("Poids Net", weightStr), 0)
                cpclPrinter.printCPCLText(0, 0, 3, 40, 330, "Auto. ONSSA : FLF.14.40.19", 0)

                try {
                    cpclPrinter.printForm()
                } catch (e: UnsupportedEncodingException) {
                    e.printStackTrace()
                }
            }
        }
    }

    private fun updateEntriesTagId() {
        if ((palletEntries as LinearLayout).childCount > 0)
            (palletEntries as LinearLayout).removeAllViews()

        for (palletEntry: PalletEntry in this.pallet!!.entries) {
            addEntry(palletEntry)
        }
    }

    private fun updatePallet(menuItem: MenuItem) {

        val palletServices = PalletsApi.create()
        val prefs = getSharedPreferences(Constants.SHARED_PREFS, 0)
        val token = prefs.getString(Constants.USER_TOKEN, "")
        menuItem.isEnabled = false

        var requiredSatisfaction = true

        if (selectedVarietyId == null) {
            varietyTextView.error = getString(R.string.required_message)
            requiredSatisfaction = false
        }

        if (processingDate.text.isEmpty()) {
            processingDate.error = getString(R.string.required_message)
            requiredSatisfaction = false
        }
        var processingDateUpdated: Date? = null
        try {
            val sdf = SimpleDateFormat(resources.getString(R.string.dateFormat))
            sdf.isLenient = true

            processingDateUpdated = sdf.parse(processingDate.text.toString())

        } catch (ex: ParseException) {
            processingDate.error = getString(R.string.invalide_date_format)
            requiredSatisfaction = false
        }


        val isVrac = vracCheckBox.isChecked


        var entriesList = ArrayList<PalletEntry>()

        for (i in 0..(palletEntries.childCount - 1)) {
            val entryView = palletEntries.getChildAt(i)


            val weightGross = entryView.weightGrossText.text.toString()
            val units = entryView.unitsText.text.toString()
            val selectedPack = selectedPackaging!![entryView.packagingTextView.tag as Int]
            if (selectedPack == null) {
                entryView.packagingTextView.error = getString(R.string.required_message)
                requiredSatisfaction = false
            }
            var calibre: String

            if (!isVrac) {
                calibre = entryView.calibreText.text.toString()
            } else {
                calibre = entryView.calibreText.text.toString()
                if (calibre.isEmpty()) {
                    calibre = "0"
                }
            }

            if (weightGross.isEmpty()) {
                entryView.weightGrossText.error = getString(R.string.required_message)
                requiredSatisfaction = false
            }


            if (units.isEmpty()) {
                entryView.unitsText.error = getString(R.string.required_message)
                requiredSatisfaction = false
            }

            if (calibre.isEmpty()) {
                entryView.calibreText.error = getString(R.string.required_message)
                requiredSatisfaction = false
            }
            if (!units.isEmpty() && !weightGross.isEmpty() && !calibre.isEmpty()) {
                val nf = NumberFormat.getInstance(Locale.ENGLISH)

                val entry = PalletEntry(id = entryView.tag as Long?,
                        units = units.toInt(),
                        weight = nf.parse(weightGross.replace(",", ".")).toDouble(),
                        calibre = calibre.toInt(),
                        weight_gross = 0.0,
                        packaging = null,
                        packaging_id = selectedPack!!.id)
                entriesList.add(entry)
            }
        }

        if (!requiredSatisfaction) {
            menuItem.isEnabled = true

            return
        }
        val progress = indeterminateProgressDialog(getString(R.string.message_loading))
        progress.setCancelable(false)
        progress.setCanceledOnTouchOutside(false)
        progress.show()

        val palletToUpdate = PalletRequestBody(
                id = pallet!!.id,
                variety_id = selectedVarietyId,
                destination_id = selectedDestinationId,
                entries = entriesList,
                entries_to_remove = entriesIdToRemove,
                processing_date = processingDateUpdated,
                vrac = isVrac
        )

        palletServices.updatePallet("token  $token", palletToUpdate)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({ pallet ->
                    this.pallet = pallet
                    updateEntriesTagId()
                    menuItem.isEnabled = true

                    progress.dismiss()

                }, { error ->
                    progress.dismiss()
                    menuItem.isEnabled = true

                    error.printStackTrace()
                })
    }

    private fun deletePallet(menuItem: MenuItem) {
        val palletServices = PalletsApi.create()
        val prefs = getSharedPreferences(Constants.SHARED_PREFS, 0)
        val token = prefs.getString(Constants.USER_TOKEN, "")
        val progress = indeterminateProgressDialog(getString(R.string.message_loading))
        progress.setCancelable(false)
        progress.setCanceledOnTouchOutside(false)
        progress.show()
        menuItem.isEnabled = false

        palletServices.deletePallet(pallet!!.id!!, "token  $token")
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({ _ ->
                    menuItem.isEnabled = true

                    progress.dismiss()
                    finish()
                }, { error ->
                    progress.dismiss()
                    menuItem.isEnabled = true

                    error.printStackTrace()
                })
    }

}
