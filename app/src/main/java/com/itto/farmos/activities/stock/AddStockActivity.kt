package com.itto.farmos.activities.stock

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.AdapterView
import com.itto.farmos.R
import com.itto.farmos.adapters.PackagingAdapter
import com.itto.farmos.adapters.ParcelAdapter
import com.itto.farmos.adapters.VarietyAdapter
import com.itto.farmos.config.Constants
import com.itto.farmos.fragments.StockFragment
import com.itto.farmos.models.Packaging
import com.itto.farmos.models.Stock
import com.itto.farmos.services.ParcelsApi
import com.itto.farmos.services.StockApi
import com.itto.farmos.services.VarietiesApi
import com.itto.farmos.tools.LoaderData
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_add_stock.*
import org.jetbrains.anko.indeterminateProgressDialog
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols
import java.text.NumberFormat
import java.util.*


class AddStockActivity : AppCompatActivity(), TextWatcher {


    private var selectedVarietyId: Long? = null
    private var selectedParcelId: Long? = null
    private var selectedPackagingId: Long? = null
    private var selectedPackaging: Packaging? = null

    companion object {
        val PRINT_STOCK = "printStock"
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_stock)

        supportActionBar!!.setHomeButtonEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)


        val varietyAdapter = VarietyAdapter(this, LoaderData.instance.varieties)
        varietyTextView.setAdapter(varietyAdapter)
        varietyTextView.threshold = 0
        weightGrossText.addTextChangedListener(this)
        unitsText.addTextChangedListener(this)
        varietyTextView.setOnItemClickListener { _: AdapterView<*>, _: View, _: Int, id: Long ->
            selectedVarietyId = id
            var variety = varietyAdapter.varieties!!.filter {
                it.id == id
            }

            parcelSpinner.text = variety[0].parcel.name


        }

        val packagingAdapter = PackagingAdapter(this, LoaderData.instance.packaging)
        packagingAdapter.packagingType = "pb"
        packagingTextView.setAdapter(packagingAdapter)
        packagingTextView.threshold = 0

        packagingTextView.setOnItemClickListener { _: AdapterView<*>, _: View, _: Int, id: Long ->
            selectedPackagingId = id
            selectedPackaging = packagingAdapter.packaging!!.filter {
                it.id == id
            }[0]
            updateWeight()
        }


    }

    private fun updateWeight(){
        if (selectedPackaging == null && weightGrossText.text.toString() == "" && unitsText.text.toString() == ""){
            weightText.text = "--"
        }else if (selectedPackaging == null && weightGrossText.text.toString() != ""){
            weightText.text = weightGrossText.text.toString()
        }else if(weightGrossText.text.toString() == "" || unitsText.text.toString() == ""){
            weightText.text = "--"
        }else{
            val weightStr = weightGrossText.text.toString()
            val nf = NumberFormat.getInstance(Locale.ENGLISH)

            val weight = nf.parse(weightStr.replace(",",".")).toDouble()
            val units = unitsText.text.toString()

            val unit = units.toInt()
            val netWeight = (weight - (unit * selectedPackaging!!.weight) - 30)

            weightText.text = DecimalFormat("#.#", DecimalFormatSymbols(Locale.ENGLISH)).format(netWeight)
        }
    }
    override fun afterTextChanged(s: Editable?) {
    }

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
    }

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
        updateWeight()
    }
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.add_button_drawer, menu)

        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item!!.itemId) {
            android.R.id.home -> {
                finish()
                return true
            }
            R.id.save -> {
                saveStock(item)
            }
        }
        return super.onOptionsItemSelected(item)
    }


    private fun saveStock(menuItem: MenuItem) {

        val stockServices = StockApi.create()
        val prefs = getSharedPreferences(Constants.SHARED_PREFS, 0)
        val token = prefs.getString(Constants.USER_TOKEN, "")
        val weight = weightGrossText.text.toString()
        val units = unitsText.text.toString()
        val temporary = temporaryCheckBox.isChecked
        val corporal = corporalText.text.toString()
        var requiredSatisfaction = true
        menuItem.isEnabled = false
        if (weight.isEmpty()) {
            weightGrossText.error = getString(R.string.required_message)
            requiredSatisfaction = false
        }

        if (units.isEmpty()) {
            unitsText.error = getString(R.string.required_message)
            requiredSatisfaction = false
        }


        if (selectedVarietyId == null) {
            varietyTextView.error = getString(R.string.required_message)
            requiredSatisfaction = false
        }

        if (selectedPackagingId == null) {
            packagingTextView.error = getString(R.string.required_message)
            requiredSatisfaction = false
        }

        if (!requiredSatisfaction) {
            menuItem.isEnabled = true

            return
        }
        val progress = indeterminateProgressDialog(getString(R.string.message_loading))
        progress.setCancelable(false)
        progress.setCanceledOnTouchOutside(false)
        progress.show()
        val nf = NumberFormat.getInstance(Locale.ENGLISH)

        val newStock = Stock(parcel_id = selectedParcelId,
                variety_id = selectedVarietyId,
                weight = nf.parse(weight.replace(",",".")).toDouble(),
                units = units.toInt(),
                parcel = null,
                variety = null,
                date = null,
                id = null,
                age = null,
                temporary = temporary,
                corporal = corporal,
                packaging = null,
                packaging_id = selectedPackagingId)

        stockServices.addStock("token  $token", newStock)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({ stock ->
                    progress.dismiss()
                    menuItem.isEnabled = true

                    val intent = Intent(this@AddStockActivity, DetailStockActivity::class.java)
                    intent.putExtra(AddStockActivity.PRINT_STOCK, stock)
                    startActivityForResult(intent, StockFragment.DETAIL_STOCK_ID)
                    finish()
                }, { error ->
                    progress.dismiss()
                    menuItem.isEnabled = true

                    error.printStackTrace()
                })
    }
}
