package com.itto.farmos.activities

import android.app.Fragment
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.view.GravityCompat
import android.support.v4.view.ViewPager
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.TextView
import com.itto.farmos.R
import com.itto.farmos.config.Constants
import com.itto.farmos.config.UserData
import com.itto.farmos.adapters.MainTabFragmentPageAdapter
import com.itto.farmos.services.UsersApi
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.*
import kotlinx.android.synthetic.main.content_main.*
import com.crashlytics.android.Crashlytics
import com.google.firebase.iid.FirebaseInstanceId
import com.itto.farmos.models.FcmToken
import com.itto.farmos.notifications.FarmOsFirebaseMessagingService
import com.itto.farmos.notifications.FirebaseIDService
import com.itto.farmos.services.FcmTokenApi
import io.fabric.sdk.android.Fabric
import io.reactivex.android.schedulers.AndroidSchedulers


class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        Fabric.with(this, Crashlytics())


        if(UserData.instance.currentUser != null) {
            val fcmTokenApi = FcmTokenApi.create()
            val prefs = getSharedPreferences(Constants.SHARED_PREFS, 0)
            val userToken = prefs.getString(Constants.USER_TOKEN, "")
            val fcmToken = FcmToken(
                    token = FirebaseInstanceId.getInstance().token,
                    user_id = UserData.instance.currentUser!!.username

            )


            fcmTokenApi.sendFcmToken("token  $userToken", fcmToken)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({ fcmTokenResponse ->
                    Log.i("token", fcmTokenResponse.status)
                }, { error ->


                    error.printStackTrace()
                })
        }

        val toggle = ActionBarDrawerToggle(
                this, drawer_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()


        val userNameText = nav_view.getHeaderView(0).findViewById<TextView>(R.id.userNameText)

        if(UserData.instance.currentUser != null) {
            userNameText.text = UserData.instance.currentUser!!.username
        }

        nav_view.setNavigationItemSelectedListener(this)

        val fragmentPageAdapter = MainTabFragmentPageAdapter(supportFragmentManager, this)
        mainViewPager.adapter = fragmentPageAdapter
//        mainViewPager.addOnPageChangeListener(object : ViewPager.SimpleOnPageChangeListener() {
//            override fun onPageSelected(position: Int) {
//                getFragmentManager().beginTransaction().detach(this).attach(this).commit();
//
//            }
//        })

        mainTabLayout.setupWithViewPager(mainViewPager)

    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menu!!.clear()

        menuInflater.inflate(R.menu.main_fragement_menu, menu)

        return true //super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item!!.itemId) {

        }
        return super.onOptionsItemSelected(item)

    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.nav_settings -> {
                val intent = Intent(this@MainActivity, SettingActivity::class.java)
                startActivity(intent)
            }
            R.id.nav_logout -> {
                val userServices = UsersApi.create()
                val prefs = this.getSharedPreferences(Constants.SHARED_PREFS, 0)
                val token = prefs.getString(Constants.USER_TOKEN, "")
                userServices.logout("token $token")
                        .subscribeOn(Schedulers.io())
                        .subscribe({ _ ->

                            val intent = Intent(this@MainActivity, LoginActivity::class.java)
                            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                            startActivity(intent)


                        }, { error ->
                            error.printStackTrace()
                        })
            }
        }

        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }
}
