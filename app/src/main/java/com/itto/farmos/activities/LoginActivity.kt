package com.itto.farmos.activities

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Button
import android.widget.EditText
import com.itto.farmos.R
import com.itto.farmos.config.Constants
import com.itto.farmos.config.UserData
import com.itto.farmos.models.User
import com.itto.farmos.services.UsersApi
import com.itto.farmos.tools.LoaderData
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import org.jetbrains.anko.indeterminateProgressDialog


class LoginActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        val userServices = UsersApi.create()

        val loginButton = findViewById<Button>(R.id.loginButton)

        val prefs = this.getSharedPreferences(Constants.SHARED_PREFS, 0)
        val lastUpdate = prefs.getString(Constants.LAST_UPDATE, "")

        loginButton.setOnClickListener {
            val userNameField = findViewById<EditText>(R.id.userNameField)
            val passwordField = findViewById<EditText>(R.id.passwordField)
            val username = userNameField.text
            val password = passwordField.text

            var requiredSatisfaction = true
            if (username == null || username.isEmpty()) {
                userNameField.error = getString(R.string.required_message)
                requiredSatisfaction = false

            }

            if (password == null || password.isEmpty()) {
                passwordField.error = getString(R.string.required_message)
                requiredSatisfaction = false
            }

            if (!requiredSatisfaction) {
                return@setOnClickListener
            }
            val progress = indeterminateProgressDialog(getString(R.string.message_loading))
            progress.setCancelable(false)
            progress.setCanceledOnTouchOutside(false)
            progress.show()
            userServices.login(username, password)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe({ tokenData ->
                        val editor = prefs!!.edit()
                        editor.putString(Constants.USER_TOKEN, tokenData.token)
                        editor.apply()

                        userServices.user("current", "token  ${tokenData.token} ")
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribeOn(Schedulers.io())
                                .subscribe({ user ->

                                    progress.dismiss()

                                    getVariety(user, lastUpdate)
                                }, { error ->
                                    error.printStackTrace()
                                    progress.dismiss()
                                })

                    }, { error ->
                        error.printStackTrace()
                        progress.dismiss()
                    })
        }

    }

    private fun getVariety(user: User, last_update: String) {
        val updateData  = last_update != user.last_update.date && user.last_update.varieties

        LoaderData.instance.loadVarieties(this@LoginActivity, updateData, {
            if (it) {
                getPackaging(user, last_update)

            }


        })
    }

    private fun getPackaging(user: User, last_update: String) {
        val updateData = last_update != user.last_update.date && user.last_update.packaging

        LoaderData.instance.loadPackaging(this@LoginActivity, updateData, {
            if (it) {
                getClients(user, last_update)

            }


        })
    }
    private fun getClients(user: User, last_update: String) {
        val updateData = last_update != user.last_update.date && user.last_update.clients

        LoaderData.instance.loadClients(this@LoginActivity, updateData, {
            if (it) {
                val prefs = this.getSharedPreferences(Constants.SHARED_PREFS, 0)
                val editor = prefs!!.edit()
                editor.putString(Constants.LAST_UPDATE, user.last_update.date)
                editor.apply()
                UserData.instance.currentUser = user
                goToMainActivity()
            }
        })

    }

    private fun goToMainActivity() {
        val intent = Intent(this@LoginActivity, MainActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(intent)
    }
}




