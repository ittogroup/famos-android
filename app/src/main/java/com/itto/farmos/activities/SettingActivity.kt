package com.itto.farmos.activities

import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.view.View

import com.itto.farmos.R
import com.itto.farmos.printer.port.bluetooth.BluetoothConnectActivity
import kotlinx.android.synthetic.main.activity_setting.*
import android.widget.RadioButton
import com.itto.farmos.config.Constants


class SettingActivity : AppCompatActivity(), View.OnClickListener {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_setting)

        supportActionBar!!.setHomeButtonEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        settingBluetoothButton.setOnClickListener(this)

        val sharedPref = this.getSharedPreferences(Constants.SHARED_PREFS, 0)
        val selectedPaperType = sharedPref.getInt(Constants.SELECTED_PAPER_TYPE, 0)
        when(selectedPaperType){
            0 -> {
                CmpLabel.isChecked = true
            }
            1 -> {
                CmpBlackMark.isChecked = true
            }
            2 -> {
                CmpContinuous.isChecked = true
            }
        }



    }

    override fun onClick(view: View?) {
        when(view!!.id){
            R.id.settingBluetoothButton -> {
                val intent = Intent(this@SettingActivity, BluetoothConnectActivity::class.java)
                startActivity(intent)
            }
            R.id.CmpLabel ->{
                setPrefInt(Constants.SELECTED_PAPER_TYPE, 0)
            }
            R.id.CmpBlackMark ->{
                setPrefInt(Constants.SELECTED_PAPER_TYPE, 1)

            }
            R.id.CmpContinuous ->{
                setPrefInt(Constants.SELECTED_PAPER_TYPE, 2)

            }
        }
    }

    private fun setPrefInt(key: String, value: Int){
        val sharedPref =  this.getSharedPreferences(Constants.SHARED_PREFS, 0)
        with(sharedPref.edit()){
            putInt(key, value)
            commit()
        }

    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item!!.itemId) {
            android.R.id.home -> {
                finish()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }
}
