package com.itto.farmos.activities

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.itto.farmos.R
import com.itto.farmos.config.Constants
import com.itto.farmos.config.UserData
import com.itto.farmos.models.User
import com.itto.farmos.services.UsersApi
import com.itto.farmos.tools.LoaderData
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers


class SplashScreenActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen_controller)
        val userServices = UsersApi.create()

        val prefs = this.getSharedPreferences(Constants.SHARED_PREFS, 0)

        val token = prefs.getString(Constants.USER_TOKEN, "")
        val lastUpdate = prefs.getString(Constants.LAST_UPDATE, "")

        userServices.user("current", "token $token")
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({ user ->

                    getVariety(user, lastUpdate)

                }, { error ->
                    error.printStackTrace()
                    val intent = Intent(this@SplashScreenActivity, LoginActivity::class.java)
                    intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                    startActivity(intent)
                })
    }

    private fun getVariety(user: User, last_update: String) {
        val updateData  = last_update != user.last_update.date && user.last_update.varieties

        LoaderData.instance.loadVarieties(this@SplashScreenActivity, updateData, {
            if (it) {
                getPackaging(user, last_update)

            }


        })
    }

    private fun getPackaging(user: User, last_update: String) {
        val updateData = last_update != user.last_update.date && user.last_update.varieties

        LoaderData.instance.loadPackaging(this@SplashScreenActivity, updateData, {
            if (it) {
                getClients(user, last_update)

            }


        })
    }

    private fun getClients(user: User, last_update: String) {
        val updateData = last_update != user.last_update.date && user.last_update.clients

        LoaderData.instance.loadClients(this@SplashScreenActivity, updateData, {
            val prefs = this.getSharedPreferences(Constants.SHARED_PREFS, 0)
            val editor = prefs!!.edit()
            editor.putString(Constants.LAST_UPDATE, user.last_update.date)
            editor.apply()
            UserData.instance.currentUser = user
            goToMainActivity()
        })

    }

    private fun goToMainActivity() {
        val intent = Intent(this@SplashScreenActivity, MainActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(intent)
    }

}
