package com.itto.farmos.activities.stock

import android.Manifest
import android.app.ProgressDialog
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.os.AsyncTask
import android.os.Build
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.text.Editable
import android.text.TextWatcher
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.AdapterView
import com.citizen.jpos.command.ESCPOSConst
import com.citizen.jpos.printer.CMPPrint
import com.citizen.jpos.printer.ESCPOSPrinter
import com.itto.farmos.R
import com.itto.farmos.adapters.ParcelAdapter
import com.itto.farmos.adapters.VarietyAdapter
import com.itto.farmos.config.Constants
import com.itto.farmos.fragments.StockFragment
import com.itto.farmos.models.Stock
import com.itto.farmos.services.ParcelsApi
import com.itto.farmos.services.StockApi
import com.itto.farmos.services.VarietiesApi
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_detail_stock.*
import org.jetbrains.anko.indeterminateProgressDialog
import android.widget.Toast
import com.citizen.jpos.command.CPCLConst
import com.citizen.jpos.command.CPCLConst.*
import com.citizen.jpos.printer.CPCLPrinter
import com.citizen.port.android.BluetoothPort
import com.citizen.port.android.WiFiPort
import com.citizen.request.android.RequestHandler
import com.itto.farmos.adapters.PackagingAdapter
import com.itto.farmos.models.Packaging
import com.itto.farmos.models.Variety
import com.itto.farmos.printer.assist.AlertView
import com.itto.farmos.tools.LoaderData
import org.jetbrains.anko.alert
import org.jetbrains.anko.noButton
import org.jetbrains.anko.yesButton
import java.io.IOException
import java.io.UnsupportedEncodingException
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols
import java.text.NumberFormat
import java.text.SimpleDateFormat
import java.time.format.DateTimeFormatter
import java.util.*


class DetailStockActivity : AppCompatActivity(), TextWatcher {

    private var selectedVarietyId: Long? = null
    private var selectedPackagingId: Long? = null
    private var stock: Stock? = null
    private var mMenu: Menu? = null
    private var context: Context? = null
    private var hThread: Thread? = null
    private var connectionTask: connTask? = null
    private var remoteDevices: Vector<BluetoothDevice>? = null
    private var selectedPackaging: Packaging? = null
    private var bluetoothPort: BluetoothPort? = null
    private var mBluetoothAdapter: BluetoothAdapter? = null
    private val REQUEST_ENABLE_BT = 2
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_stock)
        context = this

        supportActionBar!!.setHomeButtonEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        var isPrint = false
        stock = intent.getParcelableExtra<Stock>(StockFragment.DETAIL_STOCK)
        if (stock == null){
            stock = intent.getParcelableExtra<Stock>(AddStockActivity.PRINT_STOCK)
            isPrint = true
        }
        title = getString(R.string.stock) + stock!!.id

        varietyTextView.setText(stock!!.variety!!.name)
        if(stock!!.packaging != null) {
            packagingTextView.setText(stock!!.packaging!!.name)
        }
        parcelTextView.text = stock!!.variety!!.parcel!!.name
        weightGrossText.setText(stock!!.weight.toString())
        unitsText.setText(stock!!.units.toString())
        corporalText.setText(stock!!.corporal.toString())
        temporaryCheckBox.isChecked = stock!!.temporary!!
        selectedVarietyId = stock!!.variety!!.id
        if(stock!!.packaging != null){
            selectedPackagingId = stock!!.packaging!!.id
            selectedPackaging = stock!!.packaging
        }

        val netWeight = if(stock!!.packaging != null){
            (stock!!.weight!! - (stock!!.units!! * stock!!.packaging!!.weight) - 30)
        }else{
            stock!!.weight!!
        }
        weightText.text = DecimalFormat("#.#", DecimalFormatSymbols(Locale.ENGLISH)).format(netWeight)

        weightGrossText.addTextChangedListener(this)
        unitsText.addTextChangedListener(this)
        corporalText.addTextChangedListener(this)
        temporaryCheckBox.setOnClickListener {
            checkStockUpdate()
        }

        val varietyAdapter = VarietyAdapter(this, LoaderData.instance.varieties)
        varietyTextView.setAdapter(varietyAdapter)

        varietyTextView.setOnItemClickListener { _: AdapterView<*>, _: View, _: Int, id: Long ->
            selectedVarietyId = id
            var variety = varietyAdapter.varieties!!.filter {
                it.id == id
            }
            parcelTextView.text =  variety[0].parcel.name

            checkStockUpdate()
        }

        val packagingAdapter = PackagingAdapter(this, LoaderData.instance.packaging)
        packagingAdapter.packagingType = "pb"
        packagingTextView.setAdapter(packagingAdapter)
        packagingTextView.threshold = 0

        packagingTextView.setOnItemClickListener { _: AdapterView<*>, _: View, _: Int, id: Long ->
            selectedPackagingId = id
            selectedPackaging = packagingAdapter.packaging!!.filter {
                it.id == id
            }[0]
            checkStockUpdate()
        }

        clearBtDevData()
        bluetoothSetup()
        if(mBluetoothAdapter != null) {
            if (!bluetoothPort!!.isConnected) {
                var pairedDevice: BluetoothDevice
                val iter = mBluetoothAdapter!!.getBondedDevices().iterator()
                while (iter.hasNext()) {
                    pairedDevice = iter.next()
                    remoteDevices!!.add(pairedDevice)
                }
                if (remoteDevices == null || remoteDevices!!.size == 0){
                    return
                }
                pairedDevice = remoteDevices!![0]
                if (connectionTask != null && connectionTask!!.getStatus() == AsyncTask.Status.RUNNING) {
                    connectionTask!!.cancel(true)
                    if (!connectionTask!!.isCancelled())
                        connectionTask!!.cancel(true)
                    connectionTask = null
                }
                connectionTask = connTask()
                connectionTask!!.execute(pairedDevice)
            }
        }
        if(isPrint){
            alert("Do you want to print (Stock " + stock!!.id.toString() + ")") {
                yesButton {
                    print()
                }
                noButton {}
            }.show()
        }
    }

    // clear device data used list.
    private fun clearBtDevData() {
        remoteDevices = Vector()
    }


    // Set up Bluetooth.
    private fun bluetoothSetup() {
        // Initialize

        bluetoothPort = BluetoothPort.getInstance()
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter()

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val CODE = 5 // app defined constant used for onRequestPermissionsResult

            val permissionsToRequest = arrayOf(Manifest.permission.BLUETOOTH_ADMIN, Manifest.permission.BLUETOOTH, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION)

            var allPermissionsGranted = true

            for (permission in permissionsToRequest) {
                allPermissionsGranted = allPermissionsGranted && ContextCompat.checkSelfPermission(this, permission) == PackageManager.PERMISSION_GRANTED
            }

            if (!allPermissionsGranted) {
                ActivityCompat.requestPermissions(this, permissionsToRequest, CODE)
            }
        }
        if (mBluetoothAdapter == null) {
            // Device does not support Bluetooth
            return
        }
        if (!(mBluetoothAdapter?.isEnabled()!!)) {
            val enableBtIntent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT)
        }
    }

    override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
        checkStockUpdate()
    }

    override fun afterTextChanged(p0: Editable?) {
    }

    override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
    }


    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.detail_stock_drawer, menu)
        mMenu = menu
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item!!.itemId) {
            android.R.id.home -> {
                finish()
                return true
            }
            R.id.delete -> {
                alert("Are you sure you want to delete (Stock " + stock!!.id.toString() + ")") {
                    yesButton {
                        deleteStock(item)
                    }
                    noButton {}
                }.show()
                return true
            }
            R.id.print -> {
                print()
                return true
            }
            R.id.update -> {
                updateStock(item)
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun print() {
        val cpclPrinter = CPCLPrinter()

        cpclPrinter.setForm(0, 1, 1, 800, 1)
        val sharedPref =  this.getSharedPreferences(Constants.SHARED_PREFS, 0)
        val selectedPaperType = sharedPref.getInt(Constants.SELECTED_PAPER_TYPE, 0)

        cpclPrinter.setMedia(selectedPaperType)
        if(selectedPaperType == 2) {
            cpclPrinter.printBox(0, 0, 570, 740, 1)
        }
        cpclPrinter.printCPCLText(0, 5, 2, 40, 30, "Domaine Itto", 0)
        val dateFormat = SimpleDateFormat("MM/dd/yyyy HH:mm:ss")

        val formattedDate = dateFormat.format(stock!!.date)
        cpclPrinter.printCPCLText(0, 0, 3, 40, 80, "%s : %s ".format("Date", formattedDate), 0)
        cpclPrinter.printCPCLText(0, 0, 3, 40, 120, "%s : %s ".format("Variety", varietyTextView.text.toString()), 0)
        cpclPrinter.printCPCLText(0, 0, 3, 40, 160, "%s : %s ".format("Parcel", parcelTextView.text.toString()), 0)
        cpclPrinter.printCPCLText(0, 0, 3, 40, 200, "%s : %s ".format("Units", unitsText.text.toString()), 0)
        cpclPrinter.printCPCLText(0, 0, 3, 40, 240, "%s : %s ".format("Weight", weightText.text.toString()), 0)
        cpclPrinter.printCPCLText(0, 0, 3, 40, 290, "%s : %s ".format("Caporal", corporalText.text.toString()), 0)
        cpclPrinter.printCPCL2DBarCode(0, CPCLConst.CMP_CPCL_BCS_QRCODE,  40, 340, 15, 0, 1, 0, "stock-" + stock!!.id.toString())
        cpclPrinter.printCPCLText(0, 5, 2, 40, 670, "%s : %s".format("Pallet Id",  stock!!.id.toString()), 0)

        try {
            cpclPrinter.printForm()
        } catch (e: UnsupportedEncodingException) {
            e.printStackTrace()
        }
    }


    private inner class connTask : AsyncTask<BluetoothDevice, Void, Int>() {
        private val dialog = ProgressDialog(this@DetailStockActivity)

        override fun onPreExecute() {
            dialog.setTitle(resources.getString(R.string.bt_tab))
            dialog.setMessage(resources.getString(R.string.connecting_msg))
            dialog.show()
            super.onPreExecute()
        }

        override fun doInBackground(vararg params: BluetoothDevice): Int? {
            var retVal: Int? = null
            try {
                bluetoothPort!!.connect(params[0])
                retVal = Integer.valueOf(0)
            } catch (e: IOException) {
                retVal = Integer.valueOf(-1)
            }

            return retVal
        }

        override fun onPostExecute(result: Int?) {
            if (result!!.toInt() == 0)
            // Connection success.
            {
                val rh = RequestHandler()
                hThread = Thread(rh)
                hThread!!.start()

                if (dialog.isShowing)
                    dialog.dismiss()
                val toast = Toast.makeText(context, resources.getString(R.string.bt_conn_msg), Toast.LENGTH_SHORT)
                toast.show()

            } else
            // Connection failed.
            {
                if (dialog.isShowing)
                    dialog.dismiss()
                AlertView.showAlert(resources.getString(R.string.bt_conn_fail_msg),
                        resources.getString(R.string.dev_check_msg), context)
            }
            super.onPostExecute(result)
        }
    }

    private fun checkStockUpdate() {
        val weight = weightGrossText.text.toString()
        val units = unitsText.text.toString()
        val temporary = temporaryCheckBox.isChecked
        val corporal = corporalText.text.toString()

        if(units == "" || weight == "") {
            return
        }
        val nf = NumberFormat.getInstance(Locale.ENGLISH)
        val packid = if(stock!!.packaging != null){
            stock!!.packaging!!.id
        }else{
            -1
        }
        if (stock!!.weight != nf.parse(weight.replace(",",".")).toDouble() ||
                stock!!.units != units.toInt() ||
                stock!!.temporary != temporary ||
                stock!!.corporal != corporal ||
                packid != selectedPackagingId ||
                stock!!.variety!!.id != selectedVarietyId) {
            mMenu!!.findItem(R.id.delete).isVisible = false
            mMenu!!.findItem(R.id.print).isVisible = false
            mMenu!!.findItem(R.id.update).isVisible = true

            val weightStr = weightGrossText.text.toString()
            val nf = NumberFormat.getInstance(Locale.ENGLISH)

            val weight = nf.parse(weightStr.replace(",",".")).toDouble()
            val units = units.toInt()
            val netWeight = if(selectedPackaging != null){
                (weight - (units * selectedPackaging!!.weight) - 30)
            }else{
                weight
            }
            weightText.text = DecimalFormat("#.#", DecimalFormatSymbols(Locale.ENGLISH)).format(netWeight)
        } else {
            mMenu!!.findItem(R.id.delete).isVisible = true
            mMenu!!.findItem(R.id.print).isVisible = true
            mMenu!!.findItem(R.id.update).isVisible = false

        }
    }

    private fun updateStock(menuItem: MenuItem) {
        val stockServices = StockApi.create()
        val prefs = getSharedPreferences(Constants.SHARED_PREFS, 0)
        val token = prefs.getString(Constants.USER_TOKEN, "")
        menuItem.isEnabled = false

        val progress = indeterminateProgressDialog(getString(R.string.message_loading))
        progress.setCancelable(false)
        progress.setCanceledOnTouchOutside(false)
        progress.show()

        val weight = weightGrossText.text.toString()
        val units = unitsText.text.toString()
        val temporary = temporaryCheckBox.isChecked
        val corporal = corporalText.text.toString()
        val nf = NumberFormat.getInstance(Locale.ENGLISH)

        stock!!.weight = nf.parse(weight.replace(",",".")).toDouble()
        stock!!.units = units.toInt()
        stock!!.temporary = temporary
        stock!!.corporal = corporal
        stock!!.packaging_id = selectedPackagingId
        stock!!.variety_id = selectedVarietyId

        stockServices.updateStock("token  $token", stock!!.id!!, stock!!)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({ newStock ->
                    stock = newStock
                    checkStockUpdate()
                    progress.dismiss()
                    menuItem.isEnabled = true

                }, { error ->
                    progress.dismiss()
                    menuItem.isEnabled = true

                    error.printStackTrace()
                })
    }

    private fun deleteStock(menuItem: MenuItem) {
        val stockServices = StockApi.create()
        val prefs = getSharedPreferences(Constants.SHARED_PREFS, 0)
        val token = prefs.getString(Constants.USER_TOKEN, "")
        menuItem.isEnabled = false

        val progress = indeterminateProgressDialog(getString(R.string.message_loading))
        progress.setCancelable(false)
        progress.setCanceledOnTouchOutside(false)
        progress.show()

        stockServices.deleteStock(stock!!.id!!, "token  $token")
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({ _ ->
                    menuItem.isEnabled = true

                    progress.dismiss()
                    finish()
                }, { error ->
                    progress.dismiss()
                    menuItem.isEnabled = true

                    error.printStackTrace()
                })
    }
}
