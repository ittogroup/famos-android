package com.itto.farmos.activities.pallet

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.AdapterView
import com.itto.farmos.R
import com.itto.farmos.adapters.ClientAdapter
import com.itto.farmos.adapters.PackagingAdapter
import com.itto.farmos.adapters.VarietyAdapter
import com.itto.farmos.config.Constants
import com.itto.farmos.fragments.PalletFragment
import com.itto.farmos.models.Packaging
import com.itto.farmos.models.PalletEntry
import com.itto.farmos.models.PalletRequestBody
import com.itto.farmos.services.ClientsApi
import com.itto.farmos.services.PalletsApi
import com.itto.farmos.services.VarietiesApi
import com.itto.farmos.tools.LoaderData
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_add_pallet.*
import kotlinx.android.synthetic.main.view_entry_layout.view.*
import org.jetbrains.anko.indeterminateProgressDialog
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols
import java.text.NumberFormat
import java.util.*
import kotlin.collections.HashMap

class AddPalletActivity : AppCompatActivity() {

    private var selectedVarietyId: Long? = null
    private var selectedDestinationId: Long? = null
    private var selectedPackaging: HashMap<Int, Packaging>? = null
    private var selectedPackagingIndex: Int = 0
    companion object {
        val PRINT_PALLET = "printPallet"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_pallet)

        supportActionBar!!.setHomeButtonEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        selectedPackaging = HashMap()
        val varietyAdapter = VarietyAdapter(this, LoaderData.instance.varieties)
        varietyTextView.setAdapter(varietyAdapter)
        varietyTextView.threshold = 0

        val clientAdapter = ClientAdapter(this, LoaderData.instance.clients)
        destinationSpinner.setAdapter(clientAdapter)


        varietyTextView.setOnItemClickListener { _: AdapterView<*>, _: View, _: Int, id: Long ->
            selectedVarietyId = id
        }


        destinationSpinner.setOnItemClickListener { _: AdapterView<*>, _: View, _: Int, id: Long ->
            selectedDestinationId = id
        }
        vracCheckBox.setOnCheckedChangeListener { buttonView, isChecked ->
            for (i in 0..(palletEntries.childCount - 1)) {
                val entryView = palletEntries.getChildAt(i)
                if (isChecked) {
                    entryView.calibreText.visibility = View.GONE
                    entryView.calibreLabel.visibility = View.GONE
                } else {
                    entryView.calibreText.visibility = View.VISIBLE
                    entryView.calibreLabel.visibility = View.VISIBLE
                }
            }

        }
        addEntryButton.setOnClickListener {
            addEntry()
        }

        addEntry()
    }

    private fun addEntry() {
        val entryView = layoutInflater.inflate(R.layout.view_entry_layout, null)
        val longClickListener = { _: View ->

            if (entryView.removeButton.visibility == View.GONE && palletEntries.childCount > 1) {
                entryView.setBackgroundColor(resources.getColor(R.color.colorPrimaryDark))
                entryView.removeButton.visibility = View.VISIBLE
                entryView.calibreText.isEnabled = false
                entryView.unitsText.isEnabled = false
                entryView.weightText.isEnabled = false
                entryView.weightGrossText.isEnabled = false
            } else {
                entryView.removeButton.visibility = View.GONE
                entryView.setBackgroundColor(resources.getColor(R.color.whiteSmoke))
                entryView.calibreText.isEnabled = true
                entryView.unitsText.isEnabled = true
                entryView.weightText.isEnabled = true
                entryView.weightGrossText.isEnabled = true


            }

            true
        }
        entryView.calibreText.setOnLongClickListener(longClickListener)
        entryView.unitsText.setOnLongClickListener(longClickListener)
        val packagingAdapter = PackagingAdapter(this, LoaderData.instance.packaging)
        packagingAdapter.packagingType = "pf"
        entryView.packagingTextView.setAdapter(packagingAdapter)
        entryView.packagingTextView.threshold = 0
        entryView.packagingTextView.tag = selectedPackagingIndex
        entryView.packagingTextView.setOnItemClickListener { _: AdapterView<*>, _: View, _: Int, id: Long ->
            val selectedpack = packagingAdapter.packaging!!.filter {
                it.id == id
            }[0]
            selectedPackaging!![entryView.packagingTextView.tag as Int] = selectedpack
            updateWeight(entryView)
        }
        entryView.weightGrossText.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                updateWeight(entryView)
            }

        })
        entryView.unitsText.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                updateWeight(entryView)
            }
        })

        selectedPackagingIndex++

        entryView.weightGrossText.setOnLongClickListener(longClickListener)
        entryView.setOnLongClickListener(longClickListener)
        palletEntries.addView(entryView)
        var isVrac = vracCheckBox.isChecked

        if (isVrac) {
            entryView.calibreText.visibility = View.GONE
            entryView.calibreLabel.visibility = View.GONE
            entryView.calibreText.setText("0")
        } else {
            entryView.calibreText.visibility = View.VISIBLE
            entryView.calibreLabel.visibility = View.VISIBLE

        }
        entryView.removeButton.setOnClickListener {
            if (palletEntries.childCount > 1) {
                palletEntries.removeView(entryView)
            }
        }
    }

    private fun updateWeight(entryView: View){
        val selectedPack = selectedPackaging!![entryView.packagingTextView.tag as Int]
        if (selectedPack == null && entryView.weightGrossText.text.toString() == "" && entryView.unitsText.text.toString() == ""){
            entryView.weightText.text = "--"
        }else if (selectedPack == null && entryView.weightGrossText.text.toString() != ""){
            entryView.weightText.text = entryView.weightGrossText.text.toString()
        }else if(entryView.weightGrossText.text.toString() == "" || entryView.unitsText.text.toString() == ""){
            entryView.weightText.text = "--"
        }else{
            val weightStr = entryView.weightGrossText.text.toString()
            val nf = NumberFormat.getInstance(Locale.ENGLISH)

            val weight = nf.parse(weightStr.replace(",",".")).toDouble()
            val units = entryView.unitsText.text.toString()

            val unit = units.toInt()
            val netWeight = (weight - (unit * selectedPack!!.weight) - 18) as Double

            entryView.weightText.text = DecimalFormat("#.#", DecimalFormatSymbols(Locale.ENGLISH)).format(netWeight)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.add_button_drawer, menu)

        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item!!.itemId) {
            android.R.id.home -> {
                finish()
                return true
            }
            R.id.save -> {
                savePallet(item)
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun savePallet(menuItem: MenuItem) {

        val palletServices = PalletsApi.create()
        val prefs = getSharedPreferences(Constants.SHARED_PREFS, 0)
        val token = prefs.getString(Constants.USER_TOKEN, "")
        menuItem.isEnabled = false

        var requiredSatisfaction = true

        if (selectedVarietyId == null) {
            varietyTextView.error = getString(R.string.required_message)
            requiredSatisfaction = false
        }


        val isVrac = vracCheckBox.isChecked
        var entriesList = ArrayList<PalletEntry>()

        for (i in 0..(palletEntries.childCount - 1)) {
            val entryView = palletEntries.getChildAt(i)

            val weight = entryView.weightText.text.toString()
            val weightGross = entryView.weightGrossText.text.toString()

            val units = entryView.unitsText.text.toString()

            var calibre: String
            val selectedPack = selectedPackaging!![entryView.packagingTextView.tag as Int]
            if (selectedPack == null) {
                entryView.packagingTextView.error = getString(R.string.required_message)
                requiredSatisfaction = false
            }
            if (!isVrac) {
                calibre = entryView.calibreText.text.toString()
            } else {
                calibre = entryView.calibreText.text.toString()
                if (calibre.isEmpty()) {
                    calibre = "0"
                }
            }
            if (weightGross.isEmpty()) {
                entryView.weightGrossText.error = getString(R.string.required_message)
                requiredSatisfaction = false
            }


            if (weight.isEmpty()) {
                entryView.weightText.error = getString(R.string.required_message)
                requiredSatisfaction = false
            }

            if (units.isEmpty()) {
                entryView.unitsText.error = getString(R.string.required_message)
                requiredSatisfaction = false
            }

            if (calibre.isEmpty()) {
                entryView.calibreText.error = getString(R.string.required_message)
                requiredSatisfaction = false
            }
            if (!units.isEmpty() && !weight.isEmpty() && !weightGross.isEmpty() && !calibre.isEmpty()) {

                val nf = NumberFormat.getInstance(Locale.ENGLISH)
                val entry = PalletEntry(id = null,
                        units = units.toInt(),
                        weight = nf.parse(weightGross.replace(",",".")).toDouble(),
                        calibre = calibre.toInt(),
                        weight_gross = 0.0 ,packaging_id = selectedPack!!.id, packaging = null)
                entriesList.add(entry)
            } else {
                return
            }
        }

        if (!requiredSatisfaction) {
            menuItem.isEnabled = true

            return
        }
        val progress = indeterminateProgressDialog(getString(R.string.message_loading))
        progress.setCancelable(false)
        progress.setCanceledOnTouchOutside(false)
        progress.show()
        val newPallet = PalletRequestBody(
                id = null,
                variety_id = selectedVarietyId,
                destination_id = selectedDestinationId,
                entries = entriesList,
                entries_to_remove = null,
                processing_date = null,
                vrac = isVrac
        )

        palletServices.addPallet("token  $token", newPallet)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({ pallet ->

                    progress.dismiss()
                    menuItem.isEnabled = true

                    val intent = Intent(this@AddPalletActivity, DetailPalletActivity::class.java)
                    intent.putExtra(AddPalletActivity.PRINT_PALLET, pallet)
                    startActivityForResult(intent, PalletFragment.DETAIL_PALLET_ID)
                    finish()
                }, { error ->
                    progress.dismiss()
                    menuItem.isEnabled = true

                    error.printStackTrace()
                })
    }
}
