package com.itto.farmos.activities

import android.Manifest
import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.hardware.Camera
import android.os.Build
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.DisplayMetrics
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.AdapterView
import android.widget.Toast
import com.fasterxml.jackson.core.io.NumberInput

import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.GoogleApiAvailability
import com.google.android.gms.vision.MultiProcessor
import com.google.android.gms.vision.barcode.Barcode
import com.google.android.gms.vision.barcode.BarcodeDetector
import com.itto.farmos.R
import com.itto.farmos.adapters.ClientAdapter
import com.itto.farmos.adapters.PalletsBLAdapter
import com.itto.farmos.config.Constants
import com.itto.farmos.models.BL
import com.itto.farmos.models.BLCreateUpdate
import com.itto.farmos.models.Pallet
import com.itto.farmos.qrcode.barcode.BarcodeTracker
import com.itto.farmos.qrcode.barcode.BarcodeTrackerFactory
import com.itto.farmos.qrcode.camera.CameraSource
import com.itto.farmos.qrcode.camera.CameraSourcePreview
import com.itto.farmos.services.BLApi
import com.itto.farmos.services.PalletsApi
import com.itto.farmos.tools.LoaderData
import com.itto.farmos.tools.OnClickItem
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.create_update_bl.*
import org.jetbrains.anko.alert
import org.jetbrains.anko.indeterminateProgressDialog
import org.jetbrains.anko.yesButton


import java.io.IOException

class CreateUpdateBLActivity : AppCompatActivity(), BarcodeTracker.BarcodeGraphicTrackerCallback, OnClickItem {

    override fun clickItem(position: Int) {
    }

    override fun badgeUpdate() {
    }

    private var mCameraSource: CameraSource? = null
    private var mPreview: CameraSourcePreview? = null
    private lateinit var viewManager: RecyclerView.LayoutManager
    private var selectedDestinationId: Long? = null
    private var selectedBL: BL? = null
    private var blsAdapter: PalletsBLAdapter? = null
    private var palletsBLListView: RecyclerView? = null
    private var currentBlPallets: List<Pallet>? = null

    public override fun onCreate(icicle: Bundle?) {
        super.onCreate(icicle)
        setContentView(R.layout.create_update_bl)

        if(CreateUpdateBLActivity.currentBl != null){
            fetchBLPallets()
            selectedDestinationId = CreateUpdateBLActivity.currentBl!!.client!!.id
            destinationSpinner.setText( CreateUpdateBLActivity.currentBl!!.client!!.name)
        }

        blsAdapter = PalletsBLAdapter(currentBlPallets, this, this)
        supportActionBar!!.setHomeButtonEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        mPreview = findViewById(R.id.preview)
        viewManager = LinearLayoutManager(this)
        val clientAdapter = ClientAdapter(this, LoaderData.instance.clients)
        destinationSpinner.setAdapter(clientAdapter)
        palletsBLListView = findViewById<RecyclerView>(R.id.palletsBLListView).apply {

            // use a linear layout manager
            layoutManager = viewManager

            // specify an viewAdapter (see also next example)
            adapter = blsAdapter

        }
        supportActionBar!!.setHomeButtonEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        title = getString(R.string.qrcode)
        val autoFocus = true
        val useFlash = false

        val rc = ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
        if (rc == PackageManager.PERMISSION_GRANTED) {
            createCameraSource(autoFocus, useFlash)
        } else {
            requestCameraPermission()
        }
        destinationSpinner.setOnItemClickListener { _: AdapterView<*>, _: View, _: Int, id: Long ->
            selectedDestinationId = id
        }


    }

    fun fetchBLPallets() {
        val progress = this.indeterminateProgressDialog(getString(R.string.message_loading))
        progress.setCancelable(false)
        progress.setCanceledOnTouchOutside(false)
        progress.show()
        val palletServices = BLApi.create()
        val prefs = this.getSharedPreferences(Constants.SHARED_PREFS, 0)
        val token = prefs.getString(Constants.USER_TOKEN, "")

        palletServices.blPallets("token  $token", CreateUpdateBLActivity.currentBl!!.id)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({ bls ->
                    val palletsSorted = bls!!.sortedWith(compareByDescending({ it.date }))

                    blsAdapter!!.pallets = palletsSorted

                    blsAdapter!!.notifyDataSetChanged()

                    progress.dismiss()

                }, { error ->
                    error.printStackTrace()
                    progress.dismiss()
                })
    }

    override fun onDetectedQrCode(barcode: Barcode) {
        if (barcode != null) {
            this@CreateUpdateBLActivity.runOnUiThread(java.lang.Runnable {
            try {
                if(barcode.displayValue.startsWith("pallet-")) {
                    val strPalletId = barcode.displayValue.removePrefix("pallet-")

                    val palletId = NumberInput.parseLong(strPalletId)
                    if(blsAdapter!!.pallets != null) {
                        val alreadyScanned = blsAdapter!!.pallets!!.filter { it.id == palletId }.any()
                        if( alreadyScanned){
                            Toast.makeText(this@CreateUpdateBLActivity,getString(R.string.already_scanned), Toast.LENGTH_LONG ).show()
                            return@Runnable
                        }
                    }
                    val progress = this.indeterminateProgressDialog(getString(R.string.message_loading))
                    progress.setCancelable(false)
                    progress.setCanceledOnTouchOutside(false)
                    progress.show()
                    val palletServices = PalletsApi.create()
                    val prefs = this.getSharedPreferences(Constants.SHARED_PREFS, 0)
                    val token = prefs.getString(Constants.USER_TOKEN, "")
                    palletServices.getPallet(palletId, "token  $token")
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribeOn(Schedulers.io())
                            .subscribe({ pallet ->

                                blsAdapter!!.addPallet(pallet)

                                progress.dismiss()

                            }, { error ->
                                error.printStackTrace()
                                progress.dismiss()
                            })

                } else {
                    if (barcode.displayValue.startsWith("stock-")) {
                        this.alert(this.getString(R.string.use_processing_tab)) {
                            yesButton {

                            }
                        }.show()
                    } else {
                        this.alert(this.getString(R.string.invalide_qrcode)) {
                            yesButton {

                            }
                        }.show()
                    }
                }
            } catch (e: NumberFormatException) {
                e.printStackTrace()
            }
            })
        }

    }

    private fun requestCameraPermission() {
        val permissions = arrayOf(Manifest.permission.CAMERA)

        if (!ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.CAMERA)) {
            ActivityCompat.requestPermissions(this, permissions, RC_HANDLE_CAMERA_PERM)
        }
    }

    @SuppressLint("InlinedApi")
    private fun createCameraSource(autoFocus: Boolean, useFlash: Boolean) {
        val context = applicationContext

        val barcodeDetector = BarcodeDetector.Builder(context)
                .setBarcodeFormats(Barcode.ALL_FORMATS)
                .build()
        val barcodeFactory = BarcodeTrackerFactory(this)
        barcodeDetector.setProcessor(MultiProcessor.Builder(barcodeFactory).build())

        if (!barcodeDetector.isOperational) {

            val lowstorageFilter = IntentFilter(Intent.ACTION_DEVICE_STORAGE_LOW)
            val hasLowStorage = registerReceiver(null, lowstorageFilter) != null

            if (hasLowStorage) {
                Toast.makeText(this, R.string.low_storage_error,
                        Toast.LENGTH_LONG).show()
            }
        }

        val metrics = DisplayMetrics()
        windowManager.defaultDisplay.getMetrics(metrics)

        var builder: CameraSource.Builder = CameraSource.Builder(applicationContext, barcodeDetector)
                .setFacing(CameraSource.CAMERA_FACING_BACK)
                .setRequestedPreviewSize(metrics.widthPixels, metrics.heightPixels)
                .setRequestedFps(24.0f)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
            builder = builder.setFocusMode(
                    if (autoFocus) Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE else null)
        }

        mCameraSource = builder
                .setFlashMode(if (useFlash) Camera.Parameters.FLASH_MODE_TORCH else null)
                .build()
    }

    override fun onResume() {
        super.onResume()
        startCameraSource()
    }

    override fun onPause() {
        super.onPause()
        if (mPreview != null) {
            mPreview!!.stop()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        CreateUpdateBLActivity.currentBl = null
        if (mPreview != null) {
            mPreview!!.release()

        }
    }

    override fun onRequestPermissionsResult(requestCode: Int,
                                            permissions: Array<String>,
                                            grantResults: IntArray) {
        if (requestCode != RC_HANDLE_CAMERA_PERM) {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults)
            return
        }

        if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            val autoFocus = true
            val useFlash = false
            createCameraSource(autoFocus, useFlash)
            return
        }


        val listener = DialogInterface.OnClickListener { _ , _ -> finish() }

        val builder = AlertDialog.Builder(this)
        builder.setTitle("Multi-tracker")
                .setMessage(R.string.no_camera_permission)
                .setPositiveButton(R.string.ok, listener)
                .show()
    }

    @Throws(SecurityException::class)
    private fun startCameraSource() {
        val code = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(
                applicationContext)
        if (code != ConnectionResult.SUCCESS) {
            val dlg = GoogleApiAvailability.getInstance().getErrorDialog(this, code, RC_HANDLE_GMS)
            dlg.show()
        }

        if (mCameraSource != null) {
            try {
                mPreview!!.start(mCameraSource)
            } catch (e: IOException) {
                mCameraSource!!.release()
                mCameraSource = null
            }

        }
    }
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.add_button_drawer, menu)

        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item!!.itemId) {
            android.R.id.home -> {
                finish()
                return true
            }
            R.id.save -> {
                saveBL(item)
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun saveBL(menuItem: MenuItem) {
        val palletServices = BLApi.create()
        val prefs = getSharedPreferences(Constants.SHARED_PREFS, 0)
        val token = prefs.getString(Constants.USER_TOKEN, "")
        menuItem.isEnabled = false
        val progress = indeterminateProgressDialog(getString(R.string.message_loading))
        progress.setCancelable(false)
        progress.setCanceledOnTouchOutside(false)
        progress.show()
        val listPalletsIds = ArrayList<Long>()
        val listPalletIdsRemove = ArrayList<Long>()
        for (currentPallet in blsAdapter!!.pallets!!){
            listPalletsIds.add(currentPallet.id)
        }
        for (palletId in blsAdapter!!.selectedToDelete!!){

            listPalletIdsRemove.add(palletId)
        }
        var requiredSatisfaction = true

        if(selectedDestinationId == null){
                destinationSpinner.error = getString(R.string.required_message)
                requiredSatisfaction = false

        }
        if(listPalletsIds.size == 0){
            Toast.makeText(this@CreateUpdateBLActivity,getString(R.string.add_pallet_scanned), Toast.LENGTH_LONG ).show()
            requiredSatisfaction = false

        }
        if(!requiredSatisfaction){
            progress.dismiss()
            menuItem.isEnabled = true

            return
        }
        var selectedBLId: Long? = null
        if(CreateUpdateBLActivity.currentBl != null) {
            selectedBLId = CreateUpdateBLActivity.currentBl!!.id
        }
        if (selectedBL != null ){
            selectedBLId = selectedBL!!.id
        }
        val bl = BLCreateUpdate(
                client_id = selectedDestinationId!!,
                bl_id = selectedBLId,
                pallets_ids = listPalletsIds,
                pallets_ids_remove =  listPalletIdsRemove
        )

        palletServices.addBL("token  $token", bl)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({ bls ->

                    progress.dismiss()
                    menuItem.isEnabled = true

                    finish()
                }, { error ->
                    progress.dismiss()
                    menuItem.isEnabled = true

                    error.printStackTrace()
                })
    }


    companion object {

        private val TAG = "Barcode-reader"

        // Intent request code to handle updating play services if needed.
        private val RC_HANDLE_GMS = 9001

        // Permission request codes need to be < 256
        private val RC_HANDLE_CAMERA_PERM = 2

        // Constants used to pass extra data in the intent
        val BarcodeObject = "Barcode"
        var currentBl: BL? = null
    }
}
