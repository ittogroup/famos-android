package com.itto.farmos.tools

interface OnBottomReachedListener {

    fun onBottomReached(position: Int)

}