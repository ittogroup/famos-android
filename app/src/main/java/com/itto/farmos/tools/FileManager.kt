package com.itto.farmos.tools

import android.app.Activity


class FileManager{
    companion object {
        fun getAppFile(activity: Activity, fileName: String): String {
            return activity.applicationContext.filesDir.absolutePath +"/"+ fileName
        }
    }
}