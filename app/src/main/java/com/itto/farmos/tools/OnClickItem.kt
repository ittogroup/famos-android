package com.itto.farmos.tools

interface OnClickItem {
    fun clickItem(position: Int)
    fun badgeUpdate()
}