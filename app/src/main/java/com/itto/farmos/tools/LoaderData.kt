package com.itto.farmos.tools

import android.app.Activity
import android.app.ProgressDialog
import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.databind.ObjectMapper
import com.itto.farmos.R
import com.itto.farmos.config.Constants
import com.itto.farmos.models.Client
import com.itto.farmos.models.Packaging
import com.itto.farmos.models.Variety
import com.itto.farmos.services.ClientsApi
import com.itto.farmos.services.PackagingApi
import com.itto.farmos.services.VarietiesApi
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.indeterminateProgressDialog
import org.jetbrains.anko.uiThread
import java.io.File


class LoaderData private constructor() {
    var varieties: List<Variety>? = null
    var clients: List<Client>? = null
    var packaging: List<Packaging>? = null

    init {

    }

    fun loadPackaging(activity: Activity, updateData: Boolean, callBack: (Boolean) -> Unit){
        val prefs = activity.getSharedPreferences(Constants.SHARED_PREFS, 0)
        val token = prefs.getString(Constants.USER_TOKEN, "")
        val progress = activity.indeterminateProgressDialog(activity.getString(R.string.message_loading_clients))
        progress.setCancelable(false)
        progress.setCanceledOnTouchOutside(false)
        progress.show()
        if (!updateData) {
            doAsync {
                val objMapper = ObjectMapper()
                val file = File(FileManager.getAppFile(activity, "packaging.json"))

                this@LoaderData.packaging = objMapper.readValue(file, object : TypeReference<List<Packaging>>() {

                })
                uiThread {
                    callBack(true)
                    progress.dismiss()

                }
            }
            return
        }
        val packagingServices =PackagingApi.create()
        packagingServices.packaging("token  $token")
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({ packaging ->
                    this@LoaderData.packaging = packaging
                    val objMapper = ObjectMapper()
                    val file = File(FileManager.getAppFile(activity, "packaging.json"))
                    objMapper.writeValue(file, packaging)

                    callBack(true)

                    progress.dismiss()

                }, { error ->
                    callBack(false)

                    progress.dismiss()
                    error.printStackTrace()
                })
    }

    fun loadClients(activity: Activity, updateData: Boolean, callBack: (Boolean) -> Unit) {
        val prefs = activity.getSharedPreferences(Constants.SHARED_PREFS, 0)
        val token = prefs.getString(Constants.USER_TOKEN, "")
        val progress = activity.indeterminateProgressDialog(activity.getString(R.string.message_loading_clients))
        progress.setCancelable(false)
        progress.setCanceledOnTouchOutside(false)
        progress.show()
        if (!updateData) {
            doAsync {
                val objMapper = ObjectMapper()
                val file = File(FileManager.getAppFile(activity, "clients.json"))

                this@LoaderData.clients = objMapper.readValue(file, object : TypeReference<List<Client>>() {

                })
                uiThread {
                    callBack(true)
                    progress.dismiss()

                }
            }
            return
        }

        val clientsServices = ClientsApi.create()

        clientsServices.getClients("token  $token")
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({ clients ->
                    this@LoaderData.clients = clients
                    val objMapper = ObjectMapper()
                    val file = File(FileManager.getAppFile(activity, "clients.json"))
                    objMapper.writeValue(file, clients)

                    callBack(true)

                    progress.dismiss()

                }, { error ->
                    callBack(false)

                    progress.dismiss()
                    error.printStackTrace()
                })

    }

    fun loadVarieties(activity: Activity, updateData: Boolean, callBack: (Boolean) -> Unit) {
        val prefs = activity.getSharedPreferences(Constants.SHARED_PREFS, 0)
        val token = prefs.getString(Constants.USER_TOKEN, "")
        val progress = activity.indeterminateProgressDialog(activity.getString(R.string.message_loading_varieties))
        progress.setCancelable(false)
        progress.setCanceledOnTouchOutside(false)
        progress.show()
        if (!updateData) {
            doAsync {
                val objMapper = ObjectMapper()
                val file = File(FileManager.getAppFile(activity, "varieties.json"))

                this@LoaderData.varieties = objMapper.readValue(file, object : TypeReference<List<Variety>>() {

                })
                uiThread {
                    callBack(true)
                    progress.dismiss()

                }
            }
            return
        }

        val varietyServices = VarietiesApi.create()

        varietyServices.varieties("token  $token")
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({ varieties ->
                    this@LoaderData.varieties = varieties
                    val objMapper = ObjectMapper()
                    val file = File(FileManager.getAppFile(activity, "varieties.json"))
                    objMapper.writeValue(file, varieties)


                    progress.dismiss()

                    callBack(true)

                }, { error ->
                    callBack(false)

                    progress.dismiss()
                    error.printStackTrace()
                })
    }

    private object Holder {
        val INSTANCE = LoaderData()
    }

    companion object {
        val instance: LoaderData by lazy { Holder.INSTANCE }
    }
}