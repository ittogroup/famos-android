package com.itto.farmos.tools

import android.text.Html
import android.os.Build
import android.text.Spanned



class HTML{
    companion object {
        fun fromHtml(html: String): Spanned {
            return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                Html.fromHtml(html, Html.FROM_HTML_MODE_LEGACY)
            } else {
                Html.fromHtml(html)
            }
        }
    }
}