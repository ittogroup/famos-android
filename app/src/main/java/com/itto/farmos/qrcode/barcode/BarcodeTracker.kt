package com.itto.farmos.qrcode.barcode

import android.content.Context

import com.google.android.gms.vision.Tracker
import com.google.android.gms.vision.barcode.Barcode

internal class BarcodeTracker(listener: Context) : Tracker<Barcode>() {
    private val mListener: BarcodeGraphicTrackerCallback

    interface BarcodeGraphicTrackerCallback {
        fun onDetectedQrCode(barcode: Barcode)
    }

    init {
        mListener = listener as BarcodeGraphicTrackerCallback
    }

    override fun onNewItem(id: Int, item: Barcode?) {
        if (item!!.displayValue != null) {
            mListener.onDetectedQrCode(item)
        }
    }
}
