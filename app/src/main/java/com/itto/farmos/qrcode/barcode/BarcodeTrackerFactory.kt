package com.itto.farmos.qrcode.barcode

import android.content.Context

import com.google.android.gms.vision.MultiProcessor
import com.google.android.gms.vision.Tracker
import com.google.android.gms.vision.barcode.Barcode

internal class BarcodeTrackerFactory(private val mContext: Context) : MultiProcessor.Factory<Barcode> {

    override fun create(barcode: Barcode): Tracker<Barcode> {
        return BarcodeTracker(mContext)
    }
}