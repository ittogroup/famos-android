package com.itto.farmos.config

import com.itto.farmos.models.User

class UserData private constructor() {
    init {
    }

    private object Holder { val INSTANCE = UserData() }

    companion object {
        val instance: UserData by lazy { Holder.INSTANCE }
    }

    var currentUser:User? = null
}