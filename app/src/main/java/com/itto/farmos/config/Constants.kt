package com.itto.farmos.config

object Constants {
    val SHARED_PREFS = "com.itto.farmos.prefs"
    val USER_TOKEN = "UserToken"
//    val BASE_URL = "http://172.20.10.5:8000/"
//    val BASE_URL = "http://192.168.43.12:8000/"
    val BASE_URL = "https://farmos.elangr.com/"
//    val BASE_URL = "http://192.168.1.108:8000/"
//    val BASE_URL = "http://192.168.1.117:8000/"
//    val BASE_URL = "http://192.168.42.214:8000/"
    val SELECTED_PAPER_TYPE = "selectedPaperType"
    val PAGINATION_LIMIT: Int = 10
    val LAST_UPDATE = "Last_update"


}
