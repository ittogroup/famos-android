package com.itto.farmos.fragments

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.view.MenuItemCompat
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.*
import android.widget.TextView
import com.fasterxml.jackson.core.io.NumberInput
import com.google.android.gms.common.api.CommonStatusCodes
import com.google.android.gms.vision.barcode.Barcode
import com.itto.farmos.R
import com.itto.farmos.activities.pallet.AddPalletActivity
import com.itto.farmos.activities.pallet.DetailPalletActivity
import com.itto.farmos.adapters.PalletsAdapter
import com.itto.farmos.config.Constants
import com.itto.farmos.models.ArchiveItems
import com.itto.farmos.qrcode.barcode.BarcodeCaptureActivity
import com.itto.farmos.services.PalletsApi
import com.itto.farmos.tools.OnBottomReachedListener
import com.itto.farmos.tools.OnClickItem
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_pallet.view.*
import org.jetbrains.anko.alert
import org.jetbrains.anko.indeterminateProgressDialog
import org.jetbrains.anko.yesButton

class PalletFragment : Fragment(), OnClickItem, View.OnClickListener, OnBottomReachedListener {


    private var palletsAdapter: PalletsAdapter? = null
    private var palletsListView: RecyclerView? = null
    private var menuItemSearch: MenuItem? = null
    private var menuItemClearSearch: MenuItem? = null
    private var archiveItem: MenuItem? = null
    private var textCartItemCount: TextView? = null

    private var offset: Int = 0
    private var count: Int = 0

    companion object {
        val ADD_NEW_PALLET = 1100
        val DETAIL_PALLET_ID = 1101
        val DETAIL_PALLET = "detail_pallet"
    }



    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        val rootView = inflater!!.inflate(R.layout.fragment_pallet, container, false)

        palletsAdapter = PalletsAdapter(null, this!!.activity, this)
        palletsListView = rootView.palletsListView
        palletsAdapter!!.onBottomReachedListener = this
        palletsListView!!.adapter = palletsAdapter
        val stocksLayoutManager = LinearLayoutManager(activity.applicationContext)
        palletsListView!!.layoutManager = stocksLayoutManager
        palletsListView!!.itemAnimator = DefaultItemAnimator()

        fetchPallets(true)

        rootView.addPalletButton.setOnClickListener(this)
        rootView.scanPallet.setOnClickListener(this)
        setHasOptionsMenu(true)

        return rootView
    }

    override fun onBottomReached(position: Int) {
        if ((offset ) + Constants.PAGINATION_LIMIT < count) {
            offset+= Constants.PAGINATION_LIMIT
            fetchPallets(false)
        }
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
    }
    override fun onClick(view: View?) {
        when (view!!.id) {
            R.id.addPalletButton -> {
                val intent = Intent(activity, AddPalletActivity::class.java)
                startActivityForResult(intent, PalletFragment.ADD_NEW_PALLET)
            }
            R.id.scanPallet -> {
                val intent = Intent(activity, BarcodeCaptureActivity::class.java)
                startActivityForResult(intent, StockFragment.BARCODE_READER_REQUEST_CODE)
            }
        }
    }

    override fun clickItem(position: Int) {
        if(palletsAdapter!!.selectedItemCount == 0) {

            val intent = Intent(context, DetailPalletActivity::class.java)
            val pallet = palletsAdapter!!.getItem(position)
            intent.putExtra(PalletFragment.DETAIL_PALLET, pallet)
            startActivityForResult(intent, PalletFragment.DETAIL_PALLET_ID)
        }
    }
    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item!!.itemId) {
            R.id.menuItemArchive ->{
                archivePallet()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun archivePallet() {
        val progress = activity.indeterminateProgressDialog(getString(R.string.message_loading))
        progress.setCancelable(false)
        progress.setCanceledOnTouchOutside(false)
        progress.show()
        val palletServices = PalletsApi.create()
        val prefs = activity.getSharedPreferences(Constants.SHARED_PREFS, 0)
        val archiveItems = ArchiveItems(archive_ids= palletsAdapter!!.selectedArchive!!,
                status = "")
        val token = prefs.getString(Constants.USER_TOKEN, "")
        palletServices.archivePallet("token  $token", archiveItems)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({ archiveItems ->
                    palletsAdapter!!.pallets = palletsAdapter!!.pallets!!.filter { it.id.toInt() !in  archiveItems.archive_ids.toIntArray() }
                    palletsAdapter!!.notifyDataSetChanged()
                    palletsAdapter!!.selectedItemCount = 0
                    badgeUpdate()
                    progress.dismiss()

                }, { error ->
                    progress.dismiss()

                    error.printStackTrace()
                })

    }
    override fun badgeUpdate() {

        if (textCartItemCount != null) {
            if (palletsAdapter!!.selectedItemCount === 0) {
                if (textCartItemCount!!.visibility !== View.GONE) {
                    textCartItemCount!!.visibility = View.GONE
                    archiveItem!!.isVisible = false
                }
            } else {
                textCartItemCount!!.text = palletsAdapter!!.selectedItemCount.toString()
                if (textCartItemCount!!.visibility !== View.VISIBLE) {
                    textCartItemCount!!.visibility = View.VISIBLE
                    archiveItem!!.isVisible = true

                }
            }
        }
    }
    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {

        archiveItem = menu!!.findItem(R.id.menuItemArchive)
        val actionView = MenuItemCompat.getActionView(archiveItem)
        menuItemSearch = menu!!.findItem(R.id.menuItemSearch)
        menuItemSearch!!.isVisible = false
        menuItemClearSearch = menu!!.findItem(R.id.menuItemClearSearch)
        menuItemClearSearch!!.isVisible = false
        textCartItemCount = actionView!!.findViewById(R.id.cart_badge)
        badgeUpdate()
        for (i in 0 until menu!!.size()) {
            val item = menu!!.getItem(i)
            val actionView = MenuItemCompat.getActionView(item)
            actionView?.setOnClickListener { onOptionsItemSelected(item) }
        }
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == PalletFragment.ADD_NEW_PALLET || requestCode == PalletFragment.DETAIL_PALLET_ID) {
            fetchPallets(true)
        } else if (requestCode == StockFragment.BARCODE_READER_REQUEST_CODE) {
            if (resultCode === CommonStatusCodes.SUCCESS) {
                if (data != null) {
                    val barcode = data.getParcelableExtra<Barcode>(BarcodeCaptureActivity.BarcodeObject)
                    val intent = Intent(context, DetailPalletActivity::class.java)
                    try {
                        if(barcode.displayValue.startsWith("pallet-")) {
                            val strPalletId = barcode.displayValue.removePrefix("pallet-")

                        val palletId = NumberInput.parseLong(strPalletId)
                        val pallet = palletsAdapter!!.getItem(palletId)

                        if (pallet != null) {

                            intent.putExtra(PalletFragment.DETAIL_PALLET, pallet)
                            startActivityForResult(intent, PalletFragment.DETAIL_PALLET_ID)
                        }
                        } else {
                            if (barcode.displayValue.startsWith("stock-")) {
                                activity.alert(activity.getString(R.string.use_processing_tab)) {
                                    yesButton {

                                    }
                                }.show()
                            } else {
                                activity.alert(activity.getString(R.string.invalide_qrcode)) {
                                    yesButton {

                                    }
                                }.show()
                            }
                        }
                    } catch (e: NumberFormatException) {
                        e.printStackTrace()
                    }

                }
            }
        }
    }

    fun fetchPallets(clear: Boolean) {
        if (activity == null) return
        val progress = activity.indeterminateProgressDialog(getString(R.string.message_loading))
        progress.setCancelable(false)
        progress.setCanceledOnTouchOutside(false)
        progress.show()
        val palletServices = PalletsApi.create()
        val prefs = activity.getSharedPreferences(Constants.SHARED_PREFS, 0)
        val token = prefs.getString(Constants.USER_TOKEN, "")
        if (palletsAdapter!!.pallets  != null && clear ) {
            palletsAdapter!!.pallets = null
            offset = 0
            palletsListView!!.scrollTo(0,0)
        }
        palletServices.pallets("token  $token", Constants.PAGINATION_LIMIT, this.offset)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({ pallets ->
                    val palletsSorted = pallets.results!!.sortedWith(compareByDescending({ it.date }))
                    count = pallets.count!!

                    if (palletsAdapter!!.pallets == null) {
                        palletsAdapter!!.pallets = palletsSorted
                    } else {

                        palletsAdapter!!.pallets = (palletsAdapter!!.pallets!!?.plus(palletsSorted)).sortedWith(compareByDescending({ it.date }))

                    }
                    palletsAdapter!!.notifyDataSetChanged()

                    progress.dismiss()

                }, { error ->
                    error.printStackTrace()
                    progress.dismiss()
                })
    }


}