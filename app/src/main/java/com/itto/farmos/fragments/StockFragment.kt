package com.itto.farmos.fragments

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.*
import com.itto.farmos.R
import com.itto.farmos.activities.stock.AddStockActivity
import com.itto.farmos.activities.stock.DetailStockActivity
import com.itto.farmos.config.Constants
import com.itto.farmos.adapters.StocksAdapter
import com.itto.farmos.services.StockApi
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_stock.view.*
import org.jetbrains.anko.indeterminateProgressDialog
import com.google.android.gms.common.api.CommonStatusCodes
import com.fasterxml.jackson.core.io.NumberInput
import com.google.android.gms.vision.barcode.Barcode
import com.itto.farmos.qrcode.barcode.BarcodeCaptureActivity
import com.itto.farmos.tools.OnClickItem
import org.jetbrains.anko.alert
import org.jetbrains.anko.yesButton
import android.widget.TextView
import android.support.v4.view.MenuItemCompat
import com.itto.farmos.dialogs.SearchDialog
import com.itto.farmos.models.ArchiveItems
import com.itto.farmos.tools.OnBottomReachedListener
import com.itto.farmos.tools.OnSearch

class StockFragment : Fragment(), OnClickItem, View.OnClickListener, OnBottomReachedListener, OnSearch {


    companion object {
        val ADD_NEW_STOCK = 100
        val DETAIL_STOCK_ID = 101
        val BARCODE_READER_REQUEST_CODE = 102
        val DETAIL_STOCK = "detailStock"
    }


    private var offset: Int = 0
    private var offsetSearch: Int = 0
    private var count: Int = 0
    private var stockAdapter: StocksAdapter? = null
    private var stocksListView: RecyclerView? = null
    private var archiveItem: MenuItem? = null
    private var textCartItemCount: TextView? = null
    private var searchDialog: SearchDialog? = null
    private var imsearch: Boolean = false
    private var palletId: String = ""
    private var varietyId: String = ""
    private var units: String = ""
    private var weight: String = ""
    private var corporal: String = ""
    private var menuItemClearSearch: MenuItem? = null

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        val rootView = inflater!!.inflate(R.layout.fragment_stock, container, false)
        searchDialog = SearchDialog(this@StockFragment.activity)
        stockAdapter = StocksAdapter(null, activity, this)
        stockAdapter!!.onBottomReachedListener = this
        stocksListView = rootView.findViewById<RecyclerView>(R.id.stocksListView)
        if(menuItemClearSearch != null) {
            menuItemClearSearch!!.isVisible = false
        }
        imsearch = false
        stocksListView!!.recycledViewPool.clear()
        stocksListView!!.adapter = stockAdapter
        val stocksLayoutManager = LinearLayoutManager(activity.applicationContext)
        stocksListView!!.layoutManager = stocksLayoutManager
        stocksListView!!.itemAnimator = DefaultItemAnimator() as RecyclerView.ItemAnimator?

        fetchStocks(true)


        rootView.addStockButton.setOnClickListener(this)
        rootView.scanStock.setOnClickListener(this)

        return rootView
    }

    override fun seachStock(palletId: String, varietyId: String, units: String, weight: String, corporal: String) {
        menuItemClearSearch!!.isVisible = true
        val progress = activity.indeterminateProgressDialog(getString(R.string.message_loading))
        progress.setCancelable(false)
        progress.setCanceledOnTouchOutside(false)
        progress.show()
        val stockServices = StockApi.create()
        val prefs = activity.getSharedPreferences(Constants.SHARED_PREFS, 0)
        val token = prefs.getString(Constants.USER_TOKEN, "")
        this.imsearch = true
        stockAdapter!!.stocks = null
        stocksListView!!.recycledViewPool.clear()
        stocksListView!!.adapter = stockAdapter

        offsetSearch = 0
        stocksListView!!.scrollTo(0, 0)
        this.palletId = palletId
        this.varietyId = varietyId
        this.units = units
        this.weight = weight
        this.corporal = corporal
        stockServices.searchStocks("token  $token", Constants.PAGINATION_LIMIT,
                this.offsetSearch, palletId, varietyId, units, weight, corporal)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({ stocks ->
                    val stocksSorted = stocks.results!!.sortedWith(compareByDescending({ it.date }))
                    count = stocks.count!!

                    if (stockAdapter!!.stocks == null) {
                        stockAdapter!!.stocks = stocksSorted
                    } else {

                        stockAdapter!!.stocks = (stockAdapter!!.stocks!!?.plus(stocksSorted)).sortedWith(compareByDescending({ it.date }))

                    }
                    stockAdapter!!.notifyDataSetChanged()

                    progress.dismiss()

                }, { error ->
                    error.printStackTrace()
                    progress.dismiss()
                })

    }

    override fun onBottomReached(position: Int) {
        if(imsearch){
            if ((offsetSearch) + Constants.PAGINATION_LIMIT < count) {
                offsetSearch += Constants.PAGINATION_LIMIT
                seachStock(this.palletId,this.varietyId,this.units,this.weight,this.corporal)
            }
        }else {
            if ((offset) + Constants.PAGINATION_LIMIT < count) {
                offset += Constants.PAGINATION_LIMIT
                fetchStocks(false)
            }
        }
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
    }

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)

    }

    override fun onClick(view: View?) {
        when (view!!.id) {
            R.id.addStockButton -> {
                val intent = Intent(activity, AddStockActivity::class.java)
                startActivityForResult(intent, StockFragment.ADD_NEW_STOCK)
            }
            R.id.scanStock -> {
                val intent = Intent(activity, BarcodeCaptureActivity::class.java)
                startActivityForResult(intent, StockFragment.BARCODE_READER_REQUEST_CODE)
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item!!.itemId) {
            R.id.menuItemArchive -> {
                archiveStock()
            }

            R.id.menuItemSearch -> {
                searchDialog!!.searchDialog(this)
            }
            R.id.menuItemClearSearch -> {
                menuItemClearSearch!!.isVisible = false
                imsearch = false
                fetchStocks(true)

            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {

        archiveItem = menu!!.findItem(R.id.menuItemArchive)
        val actionView = MenuItemCompat.getActionView(archiveItem)
        menuItemClearSearch = menu!!.findItem(R.id.menuItemClearSearch)
        menuItemClearSearch!!.isVisible = false
        textCartItemCount = actionView!!.findViewById(R.id.cart_badge)
        badgeUpdate()
        for (i in 0 until menu!!.size()) {
            val item = menu!!.getItem(i)
            val actionView = MenuItemCompat.getActionView(item)
            actionView?.setOnClickListener { onOptionsItemSelected(item) }
        }
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun clickItem(position: Int) {
        if (stockAdapter!!.selectedItemCount == 0) {
            val intent = Intent(context, DetailStockActivity::class.java)
            val stock = stockAdapter!!.getItem(position)
            intent.putExtra(StockFragment.DETAIL_STOCK, stock)
            startActivityForResult(intent, StockFragment.DETAIL_STOCK_ID)
        }

    }

    override fun badgeUpdate() {

        if (textCartItemCount != null) {
            if (stockAdapter!!.selectedItemCount === 0) {
                if (textCartItemCount!!.visibility !== View.GONE) {
                    textCartItemCount!!.visibility = View.GONE
                    archiveItem!!.isVisible = false
                }
            } else {
                textCartItemCount!!.text = stockAdapter!!.selectedItemCount.toString()
                if (textCartItemCount!!.visibility !== View.VISIBLE) {
                    textCartItemCount!!.visibility = View.VISIBLE
                    archiveItem!!.isVisible = true

                }
            }
        }
    }

    private fun archiveStock() {
        val progress = activity.indeterminateProgressDialog(getString(R.string.message_loading))
        progress.setCancelable(false)
        progress.setCanceledOnTouchOutside(false)
        progress.show()
        val stockServices = StockApi.create()
        val prefs = activity.getSharedPreferences(Constants.SHARED_PREFS, 0)
        val archiveItems = ArchiveItems(archive_ids = stockAdapter!!.selectedArchive!!,
                status = "")
        val token = prefs.getString(Constants.USER_TOKEN, "")
        stockServices.archiveStock("token  $token", archiveItems)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({ archiveItems ->
                    stockAdapter!!.stocks = stockAdapter!!.stocks!!.filter { it.id!!.toInt() !in archiveItems.archive_ids.toIntArray() }
                    stockAdapter!!.selectedItemCount = 0
                    badgeUpdate()
                    stockAdapter!!.notifyDataSetChanged()

                    progress.dismiss()

                }, { error ->
                    progress.dismiss()

                    error.printStackTrace()
                })

    }

    public fun fetchStocks(clear: Boolean = false) {
        if (activity == null) return

        val progress = activity.indeterminateProgressDialog(getString(R.string.message_loading))
        progress.setCancelable(false)
        progress.setCanceledOnTouchOutside(false)
        progress.show()
        val stockServices = StockApi.create()
        val prefs = activity.getSharedPreferences(Constants.SHARED_PREFS, 0)
        val token = prefs.getString(Constants.USER_TOKEN, "")
        if (stockAdapter!!.stocks != null && clear) {
            stockAdapter!!.stocks = null
            stocksListView!!.recycledViewPool.clear()
            stocksListView!!.adapter = stockAdapter
            imsearch = false
            offset = 0
            stocksListView!!.scrollTo(0, 0)
        }
        stockServices.stocks("token  $token", Constants.PAGINATION_LIMIT, this.offset)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({ stocks ->
                    val stocksSorted = stocks.results!!.sortedWith(compareByDescending({ it.date }))
                    count = stocks.count!!

                    if (stockAdapter!!.stocks == null) {
                        stockAdapter!!.stocks = stocksSorted
                    } else {

                        stockAdapter!!.stocks = (stockAdapter!!.stocks!!?.plus(stocksSorted)).sortedWith(compareByDescending({ it.date }))

                    }
                    stockAdapter!!.notifyDataSetChanged()

                    progress.dismiss()

                }, { error ->
                    error.printStackTrace()
                    progress.dismiss()
                })


    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == StockFragment.ADD_NEW_STOCK || requestCode == StockFragment.DETAIL_STOCK_ID) {
           if(!imsearch) {

               fetchStocks(true)
           }

        } else if (requestCode == StockFragment.BARCODE_READER_REQUEST_CODE) {
            if (resultCode === CommonStatusCodes.SUCCESS) {
                if (data != null) {
                    val barcode = data.getParcelableExtra<Barcode>(BarcodeCaptureActivity.BarcodeObject)
                    val intent = Intent(context, DetailStockActivity::class.java)
                    try {
                        if (barcode.displayValue.startsWith("stock-")) {
                            val strStockId = barcode.displayValue.removePrefix("stock-")
                            val stockId = NumberInput.parseLong(strStockId)
                            val stock = stockAdapter!!.getItem(stockId)

                            if (stock != null) {

                                intent.putExtra(StockFragment.DETAIL_STOCK, stock)
                                startActivityForResult(intent, StockFragment.DETAIL_STOCK_ID)
                            }
                        } else {
                            if (barcode.displayValue.startsWith("pallet-")) {
                                activity.alert(activity.getString(R.string.use_pallet_tab)) {
                                    yesButton {

                                    }
                                }.show()
                            } else {
                                activity.alert(activity.getString(R.string.invalide_qrcode)) {
                                    yesButton {

                                    }
                                }.show()
                            }
                        }
                    } catch (e: NumberFormatException) {
                        e.printStackTrace()
                    }

                }
            }
        }
    }


}
