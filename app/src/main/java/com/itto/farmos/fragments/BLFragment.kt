package com.itto.farmos.fragments

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.view.MenuItemCompat
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.*
import android.widget.TextView
import com.fasterxml.jackson.core.io.NumberInput
import com.google.android.gms.common.api.CommonStatusCodes
import com.google.android.gms.vision.barcode.Barcode
import com.itto.farmos.R
import com.itto.farmos.activities.CreateUpdateBLActivity
import com.itto.farmos.activities.pallet.AddPalletActivity
import com.itto.farmos.activities.pallet.DetailPalletActivity
import com.itto.farmos.adapters.BLsAdapter
import com.itto.farmos.adapters.PalletsAdapter
import com.itto.farmos.config.Constants
import com.itto.farmos.models.ArchiveItems
import com.itto.farmos.models.BL
import com.itto.farmos.models.BLCreateUpdate
import com.itto.farmos.qrcode.barcode.BarcodeCaptureActivity
import com.itto.farmos.services.BLApi
import com.itto.farmos.services.PalletsApi
import com.itto.farmos.tools.OnBottomReachedListener
import com.itto.farmos.tools.OnClickItem
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_bl.view.*
import kotlinx.android.synthetic.main.fragment_pallet.view.*
import org.jetbrains.anko.alert
import org.jetbrains.anko.indeterminateProgressDialog
import org.jetbrains.anko.yesButton

class BLFragment : Fragment(), OnClickItem, View.OnClickListener, OnBottomReachedListener {

    private var blsAdapter: BLsAdapter? = null
    private var blsListView: RecyclerView? = null

    private var menuItemSearch: MenuItem? = null
    private var menuItemClearSearch: MenuItem? = null
    private var archiveItem: MenuItem? = null
    private var textCartItemCount: TextView? = null
    private var offset: Int = 0
    private var count: Int = 0

    companion object {
        val ADD_NEW_BL = 1200
        val DETAIL_BL_ID = 1201
        val DETAIL_BL = "detail_bl"

    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        val rootView = inflater!!.inflate(R.layout.fragment_bl, container, false)
        blsAdapter = BLsAdapter(null, this!!.activity, this)
        blsListView = rootView.blsListView
        blsAdapter!!.onBottomReachedListener = this
        blsListView!!.adapter = blsAdapter
        val stocksLayoutManager = LinearLayoutManager(activity.applicationContext)
        blsListView!!.layoutManager = stocksLayoutManager
        blsListView!!.itemAnimator = DefaultItemAnimator()

        setHasOptionsMenu(true)
        fetchBLs(true)
        rootView.addBLButton.setOnClickListener(this)
        rootView.scanBL.setOnClickListener(this)
        return rootView
    }


    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {

        archiveItem = menu!!.findItem(R.id.menuItemArchive)
        val actionView = MenuItemCompat.getActionView(archiveItem)
        menuItemSearch = menu!!.findItem(R.id.menuItemSearch)
        menuItemClearSearch = menu!!.findItem(R.id.menuItemClearSearch)
        menuItemClearSearch!!.isVisible = false
        menuItemSearch!!.isVisible = false
        textCartItemCount = actionView!!.findViewById(R.id.cart_badge)
        badgeUpdate()
        for (i in 0 until menu!!.size()) {
            val item = menu!!.getItem(i)
            val actionView = MenuItemCompat.getActionView(item)
            actionView?.setOnClickListener { onOptionsItemSelected(item) }
        }
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onBottomReached(position: Int) {
        if ((offset) + Constants.PAGINATION_LIMIT < count) {
            offset += Constants.PAGINATION_LIMIT
            fetchBLs(false)
        }
    }

    override fun onClick(view: View?) {
        when (view!!.id) {
            R.id.addBLButton -> {
                val intent = Intent(activity, CreateUpdateBLActivity::class.java)
                startActivityForResult(intent, BLFragment.ADD_NEW_BL)
            }
            R.id.scanBL -> {
                val intent = Intent(activity, BarcodeCaptureActivity::class.java)
//                startActivityForResult(intent, StockFragment.BARCODE_READER_REQUEST_CODE)
            }
        }
    }

    override fun clickItem(position: Int) {
        if (blsAdapter!!.selectedItemCount == 0) {

            val intent = Intent(context, CreateUpdateBLActivity::class.java)
            CreateUpdateBLActivity.currentBl = blsAdapter!!.getItem(position)
            startActivityForResult(intent, BLFragment.DETAIL_BL_ID)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item!!.itemId) {
            R.id.menuItemArchive -> {
//                archivePallet()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun badgeUpdate() {

        if (textCartItemCount != null) {
            textCartItemCount!!.visibility = View.GONE
            archiveItem!!.isVisible = false

        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == BLFragment.ADD_NEW_BL || requestCode == BLFragment.DETAIL_BL_ID) {
            fetchBLs(true)
        }
    }

    fun fetchBLs(clear: Boolean) {
        if (activity == null) return
        val progress = activity.indeterminateProgressDialog(getString(R.string.message_loading))
        progress.setCancelable(false)
        progress.setCanceledOnTouchOutside(false)
        progress.show()
        val palletServices = BLApi.create()
        val prefs = activity.getSharedPreferences(Constants.SHARED_PREFS, 0)
        val token = prefs.getString(Constants.USER_TOKEN, "")
        if (blsAdapter!!.bls != null && clear) {
            blsAdapter!!.bls = null
            offset = 0
            blsAdapter!!.notifyDataSetChanged()
            blsListView!!.scrollTo(0, 0)
        }
        palletServices.bls("token  $token", Constants.PAGINATION_LIMIT, this.offset)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({ bls ->
                    val palletsSorted = bls.results!!.sortedWith(compareByDescending({ it.date }))
                    count = bls.count!!

                    if (blsAdapter!!.bls == null) {
                        blsAdapter!!.bls = palletsSorted
                    } else {

                        blsAdapter!!.bls = (blsAdapter!!.bls!!?.plus(palletsSorted)).sortedWith(compareByDescending({ it.date }))

                    }
                    blsAdapter!!.notifyDataSetChanged()

                    progress.dismiss()

                }, { error ->
                    error.printStackTrace()
                    progress.dismiss()
                })
    }

}