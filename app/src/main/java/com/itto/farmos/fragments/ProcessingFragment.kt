package com.itto.farmos.fragments

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.view.MenuItemCompat
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.*
import android.view.View.OnClickListener
import android.widget.TextView
import com.fasterxml.jackson.core.io.NumberInput
import com.google.android.gms.common.api.CommonStatusCodes
import com.google.android.gms.vision.barcode.Barcode
import com.itto.farmos.R
import com.itto.farmos.adapters.ProcessedStocksAdapter
import com.itto.farmos.config.Constants
import com.itto.farmos.models.ArchiveItems
import com.itto.farmos.models.ProcessedStock
import com.itto.farmos.models.Stock
import com.itto.farmos.qrcode.barcode.BarcodeCaptureActivity
import com.itto.farmos.services.ProcessedStockApi
import com.itto.farmos.services.StockApi
import com.itto.farmos.tools.OnBottomReachedListener
import com.itto.farmos.tools.OnClickItem
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_processing.*
import kotlinx.android.synthetic.main.fragment_processing.view.*
import kotlinx.android.synthetic.main.view_processing_stock_detail.*
import kotlinx.android.synthetic.main.view_processing_stock_detail.view.*
import org.jetbrains.anko.*
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols
import java.util.*

class ProcessingFragment : Fragment(), OnClickListener, OnClickItem, OnBottomReachedListener {

    private var processedStockAdapter: ProcessedStocksAdapter? = null
    private var processedStocksListView: RecyclerView? = null
    private var selectedProcessedStock: Long? = null
    private var selectedStock: Long? = null
    private var rootView: View? = null
    private var menuItemSearch: MenuItem? = null
    private var menuItemClearSearch: MenuItem? = null
    private var offset: Int = 0
    private var count: Int = 0

    private var archiveItem: MenuItem? = null
    private var textCartItemCount: TextView? = null
    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        rootView = inflater!!.inflate(R.layout.fragment_processing, container, false)


        processedStockAdapter = ProcessedStocksAdapter(null, activity, this)
        processedStocksListView = rootView!!.findViewById<RecyclerView>(R.id.processedStocksListView)
        processedStocksListView!!.adapter = processedStockAdapter
        processedStockAdapter!!.onBottomReachedListener = this
        val stocksLayoutManager = LinearLayoutManager(activity.applicationContext)
        processedStocksListView!!.layoutManager = stocksLayoutManager
        processedStocksListView!!.itemAnimator = DefaultItemAnimator()
        fetchStocks(true)

        rootView!!.closeProcessingDetailButton.setOnClickListener(this)
        rootView!!.scanProcessingButton.setOnClickListener(this)
        rootView!!.deleteProcessingDetailButton.setOnClickListener(this)
        rootView!!.processStockButton.setOnClickListener(this)
        rootView!!.processingStockDetailView.setOnClickListener(this)
        setHasOptionsMenu(true)

        return rootView
    }

    override fun onBottomReached(position: Int) {
        if ((offset ) + Constants.PAGINATION_LIMIT < count) {
            offset+= Constants.PAGINATION_LIMIT
            fetchStocks(false)
        }
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
    }

    override fun badgeUpdate() {

        if (textCartItemCount != null) {
            if (processedStockAdapter!!.selectedItemCount === 0) {
                if (textCartItemCount!!.visibility !== View.GONE) {
                    textCartItemCount!!.visibility = View.GONE
                    archiveItem!!.isVisible = false
                }
            } else {
                textCartItemCount!!.text = processedStockAdapter!!.selectedItemCount.toString()
                if (textCartItemCount!!.visibility !== View.VISIBLE) {
                    textCartItemCount!!.visibility = View.VISIBLE
                    archiveItem!!.isVisible = true

                }
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item!!.itemId) {
            R.id.menuItemArchive ->{
                archiveProcessing()
            }
            R.id.menuItemSearch -> {

            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun archiveProcessing() {
        val progress = activity.indeterminateProgressDialog(getString(R.string.message_loading))
        progress.setCancelable(false)
        progress.setCanceledOnTouchOutside(false)
        progress.show()
        val palletServices = ProcessedStockApi.create()
        val prefs = activity.getSharedPreferences(Constants.SHARED_PREFS, 0)
        val archiveItems = ArchiveItems(archive_ids= processedStockAdapter!!.selectedArchive!!,
                status = "")
        val token = prefs.getString(Constants.USER_TOKEN, "")
        palletServices.archiveProcessedStock("token  $token", archiveItems)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({ archiveItems ->
                    processedStockAdapter!!.processedStocks = processedStockAdapter!!.processedStocks!!.filter { it.id!!.toInt() !in  archiveItems.archive_ids.toIntArray() }
                    processedStockAdapter!!.notifyDataSetChanged()
                    processedStockAdapter!!.selectedItemCount = 0
                    badgeUpdate()
                    progress.dismiss()

                }, { error ->
                    progress.dismiss()

                    error.printStackTrace()
                })
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {

        archiveItem = menu!!.findItem(R.id.menuItemArchive)
        val actionView = MenuItemCompat.getActionView(archiveItem)
        menuItemSearch = menu!!.findItem(R.id.menuItemSearch)
        menuItemSearch!!.isVisible = false
        menuItemClearSearch = menu!!.findItem(R.id.menuItemClearSearch)
        menuItemClearSearch!!.isVisible = false
        textCartItemCount = actionView!!.findViewById(R.id.cart_badge)
        badgeUpdate()
        for (i in 0 until menu!!.size()) {
            val item = menu!!.getItem(i)
            val actionView = MenuItemCompat.getActionView(item)
            actionView?.setOnClickListener { onOptionsItemSelected(item) }
        }
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun clickItem(position: Int) {
        if(processedStockAdapter!!.selectedItemCount == 0) {
            val processedStock = processedStockAdapter!!.getItem(position)
            if(processedStock != null) {
                selectedProcessedStock = processedStock!!.id
                showStock(processedStock!!.stock!!, false)
            }
        }
    }


    override fun onClick(view: View?) {
        when (view!!.id) {
            R.id.scanProcessingButton -> {
                val intent = Intent(activity, BarcodeCaptureActivity::class.java)
                startActivityForResult(intent, StockFragment.BARCODE_READER_REQUEST_CODE)
            }
            R.id.closeProcessingDetailButton -> {
                hideDetailProcessedStock()
            }
            R.id.deleteProcessingDetailButton -> {

                //TODO we need to add confirmation message before delete
                activity.alert("Are you sure you want to delete (Stock " + selectedStock!!.toString() + ")") {
                    okButton {
                        rootView!!.deleteProcessingDetailButton.isEnabled = false
                        rootView!!.deleteProcessingDetailButton.isClickable = false

                        deleteProcessedStock(selectedProcessedStock!!)
                    }
                    noButton { }
                }.show()
            }
            R.id.processStockButton -> {
                rootView!!.processStockButton.isEnabled = false
                rootView!!.processStockButton.isClickable = false
                processStock(selectedStock!!)
            }
        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == StockFragment.BARCODE_READER_REQUEST_CODE) {
            if (resultCode === CommonStatusCodes.SUCCESS) {
                if (data != null) {
                    val barcode = data.getParcelableExtra<Barcode>(BarcodeCaptureActivity.BarcodeObject)
                    try {
                        if (barcode.displayValue.startsWith("stock-")) {
                            val strStockId = barcode.displayValue.removePrefix("stock-")
                            selectedStock = NumberInput.parseLong(strStockId)
                            getStock(selectedStock!!)
                        } else {
                            if (barcode.displayValue.startsWith("pallet-")) {
                                activity.alert(activity.getString(R.string.use_pallet_tab)) {
                                    yesButton {

                                    }
                                }.show()
                            } else {
                                activity.alert(activity.getString(R.string.invalide_qrcode)) {
                                    yesButton {

                                    }
                                }.show()
                            }
                        }
                    } catch (e: NumberFormatException) {
                        e.printStackTrace()

                    }

                }
            }
        }
    }

    private fun getStock(stockId: Long) {
        val processedStock = processedStockAdapter!!.getItemByStock(stockId)

        if (processedStock != null) {
            selectedProcessedStock = processedStock!!.id
            showStock(processedStock.stock!!, false)
            return
        }

        val progress = activity.indeterminateProgressDialog(getString(R.string.message_loading))
        progress.setCancelable(false)
        progress.setCanceledOnTouchOutside(false)
        progress.show()
        val stockServices = StockApi.create()
        val prefs = activity.getSharedPreferences(Constants.SHARED_PREFS, 0)
        val token = prefs.getString(Constants.USER_TOKEN, "")
        stockServices.getStock(stockId, "token  $token")
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({ stock ->
                    showStock(stock, true)
                    progress.dismiss()
                }, { error ->
                    error.printStackTrace()
                    progress.dismiss()
                })

    }


    @Synchronized
    private fun deleteProcessedStock(processedStockId: Long) {

        val progress = activity.indeterminateProgressDialog(getString(R.string.message_loading))
        progress.setCancelable(false)
        progress.setCanceledOnTouchOutside(false)
        progress.show()
        val processedStocksServices = ProcessedStockApi.create()
        val prefs = activity.getSharedPreferences(Constants.SHARED_PREFS, 0)
        val token = prefs.getString(Constants.USER_TOKEN, "")
        processedStocksServices.deleteProcessedStock(processedStockId, "token  $token")
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({ _ ->

                    hideDetailProcessedStock()
                    rootView!!.deleteProcessingDetailButton.isEnabled = true
                    rootView!!.deleteProcessingDetailButton.isClickable = true
                    progress.dismiss()
                    fetchStocks(true)

                }, { error ->
                    error.printStackTrace()
                    progress.dismiss()
                })


    }

    @Synchronized
    private fun processStock(stockId: Long) {

        val progress = activity.indeterminateProgressDialog(getString(R.string.message_loading))
        progress.setCancelable(false)
        progress.setCanceledOnTouchOutside(false)
        progress.show()
        val processedStocksServices = ProcessedStockApi.create()
        val prefs = activity.getSharedPreferences(Constants.SHARED_PREFS, 0)
        val token = prefs.getString(Constants.USER_TOKEN, "")
        val newProcessedStock = ProcessedStock(id = null, date = null, stock = null, age = null, stock_id = stockId)
        processedStocksServices.processStock("token  $token", newProcessedStock)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({ _ ->
                    hideDetailProcessedStock()
                    rootView!!.processStockButton.isEnabled = true
                    rootView!!.processStockButton.isClickable = true
                    progress.dismiss()
                    fetchStocks(true)
                }, { error ->
                    error.printStackTrace()
                    progress.dismiss()
                })


    }

    public fun fetchStocks(clear: Boolean) {
        if (activity == null) return

        val progress = activity.indeterminateProgressDialog(getString(R.string.message_loading))
        progress.setCancelable(false)
        progress.setCanceledOnTouchOutside(false)
        progress.show()
        val processedStocksServices = ProcessedStockApi.create()
        val prefs = activity.getSharedPreferences(Constants.SHARED_PREFS, 0)
        val token = prefs.getString(Constants.USER_TOKEN, "")
        if (processedStockAdapter!!.processedStocks  != null && clear ) {
            processedStockAdapter!!.processedStocks = null
            offset = 0
            processedStocksListView!!.scrollTo(0, 0)
        }
        processedStocksServices.processed_stocks("token  $token", Constants.PAGINATION_LIMIT, this.offset)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({ processedStocks ->
                    val stocksSorted = processedStocks.results!!.sortedWith(compareByDescending({ it.date }))

                    count = processedStocks.count!!

                    if (processedStockAdapter!!.processedStocks == null) {
                        processedStockAdapter!!.processedStocks = stocksSorted
                    } else {

                        processedStockAdapter!!.processedStocks = (processedStockAdapter!!.processedStocks!!?.plus(stocksSorted)).sortedWith(compareByDescending({ it.date }))

                    }
                    processedStockAdapter!!.notifyDataSetChanged()

                    progress.dismiss()

                }, { error ->
                    error.printStackTrace()
                    progress.dismiss()
                })


    }

    private fun showStock(stock: Stock, isProcessing: Boolean) {
        varietyTextView.text = stock.variety!!.name
        parcelTextView.text = stock.variety!!.parcel!!.name
        unitsText.text = stock.units.toString()

        val netWeight = if(stock.packaging != null && stock.units != null && stock.weight != null){
            (stock.weight as Double - (stock.units as Int * stock.packaging!!.weight) - 30)
        }else{
            stock.weight
        }
        weightText.text = DecimalFormat("#.#", DecimalFormatSymbols(Locale.ENGLISH)).format(netWeight) + "Kg"


        if (isProcessing) {
            deleteProcessingDetailButton.visibility = View.GONE
            processStockButton.visibility = View.VISIBLE
        } else {
            processStockButton.visibility = View.GONE
            deleteProcessingDetailButton.visibility = View.VISIBLE
        }

        showDetailProcessedStock()
    }

    private fun showDetailProcessedStock() {

        scanProcessingButton.visibility = View.GONE
        processingStockDetailView.visibility = View.VISIBLE
    }

    private fun hideDetailProcessedStock() {

        scanProcessingButton.visibility = View.VISIBLE
        processingStockDetailView.visibility = View.GONE
    }
}